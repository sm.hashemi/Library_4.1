
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JTextArea;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JScrollPane;

public class About {

	private static JFrame frameAbout;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		InitializeFrameAbout();
		frameAbout.setVisible(true);

	}

	/**
	 * Initialize the frame.
	 */
	private static void InitializeFrameAbout() {
		frameAbout = new JFrame();
		frameAbout.setBounds(100, 100, 472, 619);
		frameAbout.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 320, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 510, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		frameAbout.getContentPane().setLayout(gridBagLayout);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 1;
		frameAbout.getContentPane().add(scrollPane, gbc_scrollPane);

		JTextArea txtrAbout = new JTextArea();
		txtrAbout.setText(
				"\r\n                                            ** Creators **             \r\n                        Seyyed Mahdi Hashemi Fesharaki    \r\n                       Professor Haj Mohammad Hamzeii\r\n\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\r\n                                         ** Change Log **            \r\n                  Beta  (95/12/11)\r\n                       App created\r\n                       CRUD method added\r\n                  1.0.0 (95/12/12)\r\n                       First release\r\n                       Splitted Main class\r\n                       Improved CRUD method\r\n                       Added Advanced search\r\n                       Added Password protection\r\n                       Added About app\r\n                       Added Comments\r\n                       Improved design\r\n                       Bug fixes\r\n                  1.1.0 (95/12/13)\r\n                       Added arrays cleaner\r\n                       Added arrays max increaser\r\n                       Bug Fixes\r\n                  1.2.0 (95/12/19)\r\n                       Changed Delete method\r\n                       Bug fixes\r\n                  2.0.0 (96/01/07)\r\n                       App Reborn !\r\n                       App Object Oreinted\r\n                       Added Book managment\r\n                       Added Dual login\r\n                       Added Borrow/Return Book\r\n                       Improved Advanced search\r\n                       Bug fixes\r\n                  2.1.0 (96/01/08)\r\n                       Improved design\r\n                       Bug fixes\r\n                  3.0.0 (96/02/02)\r\n                       Design Revolation!\r\n                       Added JFrames\r\n                       Package division\r\n                       Added Library class\r\n                       Added Person superclass\r\n                       Used Inheritance\r\n                       Added Menus header\r\n                       Improved Advanced search\r\n                       View Full Books list\r\n                       View Full Members list\r\n                       View Borrowers list\r\n                       View Delayers list        \r\n                       Save data in XML           \r\n                  4.0.0 (96/03/09)\r\n                       Replaced Arrays with LinkedList\r\n                       Added Listable interface\r\n                       B&M implements Listable\r\n                       B&M implements Serializable\r\n                       Added File I/O\r\n                       Removed XML saving\r\n                       Bug fixes\r\n                  4.1.0 (96/03/10)\r\n                       Added logging\r\n                       Penalty managment\r\n                       Bug fixes\r\n                  Soon\r\n                       Export App\r\n                       Setting key\r\n\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\r\n                                          ** Contact US **            \r\n                       Yahoo       :  sm.hf                  \r\n                       Telegram :  sm.hf                  \r\n                       Telegram :  mohammadhamzei\r\n");
		txtrAbout.setEditable(false);
		scrollPane.setViewportView(txtrAbout);
	}

}