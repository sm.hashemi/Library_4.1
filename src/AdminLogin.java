
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.SwingConstants;

import model.*;

import java.awt.Window.Type;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminLogin {

	private static JFrame frameAdminLogin;
	private static JTextField fieldAdminId;
	private static JPasswordField fieldAdminPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		InitializeFrameAdminLogin();
		frameAdminLogin.setVisible(true);
	}

	/**
	 * Initialize the frame.
	 */
	private static void InitializeFrameAdminLogin() {
		frameAdminLogin = new JFrame();
		frameAdminLogin.setResizable(false);
		frameAdminLogin.setType(Type.POPUP);
		frameAdminLogin.setTitle("Admin Login");
		frameAdminLogin.setBounds(100, 100, 330, 230);
		frameAdminLogin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 50, 0, 0, 94, 50, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frameAdminLogin.getContentPane().setLayout(gridBagLayout);

		JLabel lblAdminId = new JLabel("Admin ID:");
		lblAdminId.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblAdminId = new GridBagConstraints();
		gbc_lblAdminId.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAdminId.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdminId.gridx = 1;
		gbc_lblAdminId.gridy = 1;
		frameAdminLogin.getContentPane().add(lblAdminId, gbc_lblAdminId);

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 2;
		gbc_verticalStrut.gridy = 1;
		frameAdminLogin.getContentPane().add(verticalStrut, gbc_verticalStrut);

		fieldAdminId = new JTextField();
		fieldAdminId.setEditable(false);
		fieldAdminId.setText("admin");
		GridBagConstraints gbc_fieldAdminId = new GridBagConstraints();
		gbc_fieldAdminId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldAdminId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldAdminId.gridx = 3;
		gbc_fieldAdminId.gridy = 1;
		frameAdminLogin.getContentPane().add(fieldAdminId, gbc_fieldAdminId);
		fieldAdminId.setColumns(10);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 2;
		frameAdminLogin.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 1;
		gbc_horizontalStrut_1.gridy = 3;
		frameAdminLogin.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		JLabel lblAdminPassword = new JLabel("Password:");
		lblAdminPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblAdminPassword = new GridBagConstraints();
		gbc_lblAdminPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAdminPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdminPassword.gridx = 1;
		gbc_lblAdminPassword.gridy = 4;
		frameAdminLogin.getContentPane().add(lblAdminPassword, gbc_lblAdminPassword);

		fieldAdminPassword = new JPasswordField();
		GridBagConstraints gbc_fieldAdminPassword = new GridBagConstraints();
		gbc_fieldAdminPassword.insets = new Insets(0, 0, 5, 5);
		gbc_fieldAdminPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldAdminPassword.gridx = 3;
		gbc_fieldAdminPassword.gridy = 4;
		frameAdminLogin.getContentPane().add(fieldAdminPassword, gbc_fieldAdminPassword);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 3;
		gbc_horizontalStrut_4.gridy = 5;
		frameAdminLogin.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 3;
		gbc_horizontalStrut_5.gridy = 6;
		frameAdminLogin.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 3;
		gbc_horizontalStrut_6.gridy = 7;
		frameAdminLogin.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 3;
		gbc_horizontalStrut_3.gridy = 8;
		frameAdminLogin.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 3;
		gbc_horizontalStrut_2.gridy = 9;
		frameAdminLogin.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String password = String.valueOf(fieldAdminPassword.getPassword());
				if (adminValidate(password)) {
					Main.disposeFrame();
					frameAdminLogin.dispose();
					AdminMenu.main(null);
				}
			}
		});
		btnLogin.setPreferredSize(new Dimension(70, 25));
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.gridwidth = 3;
		gbc_btnLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogin.gridx = 1;
		gbc_btnLogin.gridy = 10;
		frameAdminLogin.getContentPane().add(btnLogin, gbc_btnLogin);
	}

	/**
	 * Validate administrator by password
	 */
	public static boolean adminValidate(String password) { // 888
		if (password.equals(Library.getAdminPassword()))
			return true;
		else {
			Library.alert("Wrong password!", "Admin Login Error", true, false);
			return false;
		}
	}
}
