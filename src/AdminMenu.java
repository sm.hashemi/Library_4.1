
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.border.LineBorder;

import model.Book;
import model.Library;
import model.Member;
import options.*;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminMenu {

	private static JFrame frameAdmin;
	private static boolean hasDelayer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameAdminMenu();
		frameAdmin.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameAdminMenu() {
		frameAdmin = new JFrame();
		frameAdmin.setResizable(false);
		frameAdmin.setTitle("Admin Menu");
		frameAdmin.setVisible(true);
		frameAdmin.setBounds(100, 100, 472, 672);
		frameAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 50, 0, 0, 0, 50, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		frameAdmin.getContentPane().setLayout(gridBagLayout);

		JLabel lblHello = new JLabel("  Hello Administrator!");
		lblHello.setForeground(new Color(0, 153, 0));
		lblHello.setMaximumSize(new Dimension(140, 14));
		lblHello.setMinimumSize(new Dimension(323, 22));
		lblHello.setPreferredSize(new Dimension(140, 18));
		lblHello.setBorder(new LineBorder(new Color(0, 204, 0)));
		GridBagConstraints gbc_lblHello = new GridBagConstraints();
		gbc_lblHello.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblHello.gridwidth = 3;
		gbc_lblHello.insets = new Insets(0, 0, 5, 5);
		gbc_lblHello.gridx = 1;
		gbc_lblHello.gridy = 1;
		frameAdmin.getContentPane().add(lblHello, gbc_lblHello);

		String nowDate = new SimpleDateFormat("EEE yyyy-MM-dd HH:mm").format(new Date());
		JLabel lblToday = new JLabel("  Today " + nowDate);
		lblToday.setForeground(new Color(0, 153, 0));
		lblToday.setBorder(new LineBorder(new Color(0, 204, 0)));
		lblToday.setMinimumSize(new Dimension(323, 22));
		GridBagConstraints gbc_lblToday = new GridBagConstraints();
		gbc_lblToday.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblToday.gridwidth = 3;
		gbc_lblToday.insets = new Insets(0, 0, 5, 5);
		gbc_lblToday.gridx = 1;
		gbc_lblToday.gridy = 2;
		frameAdmin.getContentPane().add(lblToday, gbc_lblToday);

		// is it any delayer?
		for (int i = 0; Library.member.getObject(i) != null; i++) {
			Member m = (Member) Library.member.getObject(i);
			if (m.isBorrower())
				if (m.isDelayer()) {
					hasDelayer = true;
					break;
				}
			hasDelayer=false;
		}
		
		
		if (hasDelayer) {
			JLabel lblNews = new JLabel("  WARNING! Some Members have delay in returning Books!");
			lblNews.setForeground(new Color(255, 0, 0));
			lblNews.setBorder(new LineBorder(new Color(255, 69, 0)));
			lblNews.setMinimumSize(new Dimension(323, 22));
			GridBagConstraints gbc_lblNews = new GridBagConstraints();
			gbc_lblNews.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblNews.gridwidth = 3;
			gbc_lblNews.insets = new Insets(0, 0, 5, 5);
			gbc_lblNews.gridx = 1;
			gbc_lblNews.gridy = 3;
			frameAdmin.getContentPane().add(lblNews, gbc_lblNews);
			frameAdmin.getContentPane().add(lblNews, gbc_lblNews);
		} else {
			JLabel lblNews = new JLabel("  Good News! All Books are in the currect state!");
			lblNews.setForeground(new Color(0, 153, 0));
			lblNews.setBorder(new LineBorder(new Color(0, 204, 0)));
			lblNews.setMinimumSize(new Dimension(323, 22));
			GridBagConstraints gbc_lblNews = new GridBagConstraints();
			gbc_lblNews.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblNews.gridwidth = 3;
			gbc_lblNews.insets = new Insets(0, 0, 5, 5);
			gbc_lblNews.gridx = 1;
			gbc_lblNews.gridy = 3;
			frameAdmin.getContentPane().add(lblNews, gbc_lblNews);
		}

		JButton btnCreateMember = new JButton("\uD83D\uDC64  Add Member");
		btnCreateMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MemberAdd.main(null);
			}
		});
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 3;
		frameAdmin.getContentPane().add(verticalStrut, gbc_verticalStrut);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_2.gridx = 1;
		gbc_verticalStrut_2.gridy = 4;
		frameAdmin.getContentPane().add(verticalStrut_2, gbc_verticalStrut_2);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_1.gridx = 1;
		gbc_verticalStrut_1.gridy = 5;
		frameAdmin.getContentPane().add(verticalStrut_1, gbc_verticalStrut_1);
		btnCreateMember.setMinimumSize(new Dimension(150, 30));
		btnCreateMember.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnCreateMember = new GridBagConstraints();
		gbc_btnCreateMember.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCreateMember.insets = new Insets(0, 0, 5, 5);
		gbc_btnCreateMember.gridx = 1;
		gbc_btnCreateMember.gridy = 6;
		frameAdmin.getContentPane().add(btnCreateMember, gbc_btnCreateMember);

		JButton btnCreateBook = new JButton("\uD83D\uDCD6  Add Book");
		btnCreateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookAdd.main(null);
			}
		});
		btnCreateBook.setMinimumSize(new Dimension(150, 30));
		btnCreateBook.setPreferredSize(new Dimension(150, 30));
		GridBagConstraints gbc_btnCreateBook = new GridBagConstraints();
		gbc_btnCreateBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCreateBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnCreateBook.gridx = 3;
		gbc_btnCreateBook.gridy = 6;
		frameAdmin.getContentPane().add(btnCreateBook, gbc_btnCreateBook);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 1;
		gbc_horizontalStrut_3.gridy = 7;
		frameAdmin.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 8;
		frameAdmin.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		Component horizontalStrut_18 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_18 = new GridBagConstraints();
		gbc_horizontalStrut_18.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_18.gridx = 1;
		gbc_horizontalStrut_18.gridy = 9;
		frameAdmin.getContentPane().add(horizontalStrut_18, gbc_horizontalStrut_18);

		JButton btnSearchMember = new JButton("\uD83D\uDD0E  Search Member");
		btnSearchMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MemberSearch.main(null);
			}
		});
		btnSearchMember.setMinimumSize(new Dimension(140, 30));
		btnSearchMember.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnSearchMember = new GridBagConstraints();
		gbc_btnSearchMember.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSearchMember.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearchMember.gridx = 1;
		gbc_btnSearchMember.gridy = 10;
		frameAdmin.getContentPane().add(btnSearchMember, gbc_btnSearchMember);

		JButton btnSearchBook = new JButton("\uD83D\uDD0E  Search Book");
		btnSearchBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				BookSearch.main(null);
			}
		});
		btnSearchBook.setMinimumSize(new Dimension(140, 30));
		btnSearchBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnSearchBook = new GridBagConstraints();
		gbc_btnSearchBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSearchBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearchBook.gridx = 3;
		gbc_btnSearchBook.gridy = 10;
		frameAdmin.getContentPane().add(btnSearchBook, gbc_btnSearchBook);

		Component horizontalStrut_19 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_19 = new GridBagConstraints();
		gbc_horizontalStrut_19.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_19.gridx = 1;
		gbc_horizontalStrut_19.gridy = 11;
		frameAdmin.getContentPane().add(horizontalStrut_19, gbc_horizontalStrut_19);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 1;
		gbc_horizontalStrut_4.gridy = 12;
		frameAdmin.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 1;
		gbc_horizontalStrut_1.gridy = 13;
		frameAdmin.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		JButton btnUpdateMember = new JButton("\uD83D\uDD03  Update Member");
		btnUpdateMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MemberUpdate.main(null);
			}
		});
		btnUpdateMember.setMinimumSize(new Dimension(140, 30));
		btnUpdateMember.setPreferredSize(new Dimension(150, 30));
		GridBagConstraints gbc_btnUpdateMember = new GridBagConstraints();
		gbc_btnUpdateMember.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUpdateMember.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdateMember.gridx = 1;
		gbc_btnUpdateMember.gridy = 14;
		frameAdmin.getContentPane().add(btnUpdateMember, gbc_btnUpdateMember);

		JButton btnUpdateBook = new JButton("\uD83D\uDD03  Update Book");
		btnUpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				BookUpdate.main(null);
			}
		});
		btnUpdateBook.setMinimumSize(new Dimension(140, 30));
		btnUpdateBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnUpdateBook = new GridBagConstraints();
		gbc_btnUpdateBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUpdateBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdateBook.gridx = 3;
		gbc_btnUpdateBook.gridy = 14;
		frameAdmin.getContentPane().add(btnUpdateBook, gbc_btnUpdateBook);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 1;
		gbc_horizontalStrut_5.gridy = 15;
		frameAdmin.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_20 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_20 = new GridBagConstraints();
		gbc_horizontalStrut_20.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_20.gridx = 1;
		gbc_horizontalStrut_20.gridy = 16;
		frameAdmin.getContentPane().add(horizontalStrut_20, gbc_horizontalStrut_20);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 17;
		frameAdmin.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		JButton btnDeletedMember = new JButton("\u2716  Delete Member");
		btnDeletedMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MemberDelete.main(null);
			}
		});
		btnDeletedMember.setMinimumSize(new Dimension(140, 30));
		btnDeletedMember.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnDeletedMember = new GridBagConstraints();
		gbc_btnDeletedMember.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeletedMember.insets = new Insets(0, 0, 5, 5);
		gbc_btnDeletedMember.gridx = 1;
		gbc_btnDeletedMember.gridy = 18;
		frameAdmin.getContentPane().add(btnDeletedMember, gbc_btnDeletedMember);

		JButton btnDeleteBook = new JButton("\u2716  Delete Book");
		btnDeleteBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				BookDelete.main(null);
			}
		});
		btnDeleteBook.setMinimumSize(new Dimension(140, 30));
		btnDeleteBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnDeleteBook = new GridBagConstraints();
		gbc_btnDeleteBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeleteBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnDeleteBook.gridx = 3;
		gbc_btnDeleteBook.gridy = 18;
		frameAdmin.getContentPane().add(btnDeleteBook, gbc_btnDeleteBook);

		JLabel label = new JLabel("_________________________________________________");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridwidth = 3;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 19;
		frameAdmin.getContentPane().add(label, gbc_label);

		JButton btnMembersList = new JButton("\uD83D\uDCC6  Members List");
		btnMembersList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MembersList.resetTable();
				for (int i = 0; Library.member.getObject(i) != null; i++) {
					MembersList.addTableRow((Member) Library.member.getObject(i));
				}
				MembersList.main(null);
			}
		});

		Component horizontalStrut_23 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_23 = new GridBagConstraints();
		gbc_horizontalStrut_23.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_23.gridx = 1;
		gbc_horizontalStrut_23.gridy = 20;
		frameAdmin.getContentPane().add(horizontalStrut_23, gbc_horizontalStrut_23);

		Component horizontalStrut_21 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_21 = new GridBagConstraints();
		gbc_horizontalStrut_21.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_21.gridx = 1;
		gbc_horizontalStrut_21.gridy = 21;
		frameAdmin.getContentPane().add(horizontalStrut_21, gbc_horizontalStrut_21);
		btnMembersList.setMaximumSize(new Dimension(130, 40));
		btnMembersList.setMinimumSize(new Dimension(140, 30));
		btnMembersList.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnMembersList = new GridBagConstraints();
		gbc_btnMembersList.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnMembersList.insets = new Insets(0, 0, 5, 5);
		gbc_btnMembersList.gridx = 1;
		gbc_btnMembersList.gridy = 22;
		frameAdmin.getContentPane().add(btnMembersList, gbc_btnMembersList);

		JButton btnBooksList = new JButton("\uD83D\uDCC6  Books List");
		btnBooksList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				BooksList.resetTable();
				for (int i = 0; Library.book.getObject(i) != null; i++) {
					BooksList.addTableRow((Book) Library.book.getObject(i));
				}
				BooksList.main(null);
			}
		});
		GridBagConstraints gbc_btnBooksList = new GridBagConstraints();
		gbc_btnBooksList.fill = GridBagConstraints.BOTH;
		gbc_btnBooksList.insets = new Insets(0, 0, 5, 5);
		gbc_btnBooksList.gridx = 3;
		gbc_btnBooksList.gridy = 22;
		frameAdmin.getContentPane().add(btnBooksList, gbc_btnBooksList);

		Component horizontalStrut_16 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_16 = new GridBagConstraints();
		gbc_horizontalStrut_16.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_16.gridx = 1;
		gbc_horizontalStrut_16.gridy = 23;
		frameAdmin.getContentPane().add(horizontalStrut_16, gbc_horizontalStrut_16);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 1;
		gbc_horizontalStrut_9.gridy = 24;
		frameAdmin.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 1;
		gbc_horizontalStrut_8.gridy = 25;
		frameAdmin.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		JButton btnBorrowersList = new JButton("\u23F0  Borrowers List");
		btnBorrowersList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MembersList.resetTable();
				for (int i = 0; Library.member.getObject(i) != null; i++) {
					Member m = (Member) Library.member.getObject(i);
					if (m.isBorrower())
						MembersList.addTableRow(m);
				}
				MembersList.main(null);
			}
		});
		btnBorrowersList.setPreferredSize(new Dimension(140, 30));
		btnBorrowersList.setMinimumSize(new Dimension(140, 30));
		btnBorrowersList.setMaximumSize(new Dimension(130, 40));
		GridBagConstraints gbc_btnBorrowersList = new GridBagConstraints();
		gbc_btnBorrowersList.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnBorrowersList.insets = new Insets(0, 0, 5, 5);
		gbc_btnBorrowersList.gridx = 1;
		gbc_btnBorrowersList.gridy = 26;
		frameAdmin.getContentPane().add(btnBorrowersList, gbc_btnBorrowersList);

		JButton btnDelayersList = new JButton("\uD83D\uDCDE Delayers List");
		btnDelayersList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				MembersList.resetTable();
				for (int i = 0; Library.member.getObject(i) != null; i++) {
					Member m = (Member) Library.member.getObject(i);
					if (m.isBorrower())
						if (m.isDelayer())
							MembersList.addTableRow(m);
				}
				MembersList.main(null);
			}
		});
		btnDelayersList.setPreferredSize(new Dimension(140, 30));
		btnDelayersList.setMinimumSize(new Dimension(140, 30));
		btnDelayersList.setMaximumSize(new Dimension(130, 40));
		GridBagConstraints gbc_btnDelayersList = new GridBagConstraints();
		gbc_btnDelayersList.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDelayersList.insets = new Insets(0, 0, 5, 5);
		gbc_btnDelayersList.gridx = 3;
		gbc_btnDelayersList.gridy = 26;
		frameAdmin.getContentPane().add(btnDelayersList, gbc_btnDelayersList);

		JLabel lblNewLabel = new JLabel("_________________________________________________");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 27;
		frameAdmin.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 3;
		gbc_horizontalStrut_6.gridy = 28;
		frameAdmin.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_22 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_22 = new GridBagConstraints();
		gbc_horizontalStrut_22.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_22.gridx = 1;
		gbc_horizontalStrut_22.gridy = 29;
		frameAdmin.getContentPane().add(horizontalStrut_22, gbc_horizontalStrut_22);

		JButton btnBorrowBook = new JButton("\uD83D\uDCE4  Borrow Book");
		btnBorrowBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				Borrow.main(null);
			}
		});
		btnBorrowBook.setMinimumSize(new Dimension(140, 30));
		btnBorrowBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnBorrowBook = new GridBagConstraints();
		gbc_btnBorrowBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnBorrowBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnBorrowBook.gridx = 1;
		gbc_btnBorrowBook.gridy = 30;
		frameAdmin.getContentPane().add(btnBorrowBook, gbc_btnBorrowBook);

		JButton btnReturnBook = new JButton("\uD83D\uDCE5  Return Book");
		btnReturnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (Library.member.isEmpty()) {
					Library.alert("No Member exist!", "Error!", true, true);
					return;
				}
				if (Library.book.isEmpty()) {
					Library.alert("No Book exist!", "Error!", true, true);
					return;
				}
				Return.main(null);
			}
		});
		btnReturnBook.setMinimumSize(new Dimension(140, 30));
		btnReturnBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnReturnBook = new GridBagConstraints();
		gbc_btnReturnBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnReturnBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnReturnBook.gridx = 3;
		gbc_btnReturnBook.gridy = 30;
		frameAdmin.getContentPane().add(btnReturnBook, gbc_btnReturnBook);

		JLabel lblNewLabel_1 = new JLabel("_________________________________________________");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridwidth = 3;
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 31;
		frameAdmin.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		Component horizontalStrut_24 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_24 = new GridBagConstraints();
		gbc_horizontalStrut_24.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_24.gridx = 1;
		gbc_horizontalStrut_24.gridy = 32;
		frameAdmin.getContentPane().add(horizontalStrut_24, gbc_horizontalStrut_24);

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 1;
		gbc_horizontalStrut_7.gridy = 33;
		frameAdmin.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JButton btnChangePassword = new JButton("\uD83D\uDD13  Change Password");
		btnChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SelfAdminPass.main(null);
			}
		});
		btnChangePassword.setPreferredSize(new Dimension(140, 30));
		btnChangePassword.setMinimumSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnChangePassword = new GridBagConstraints();
		gbc_btnChangePassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnChangePassword.insets = new Insets(0, 0, 5, 5);
		gbc_btnChangePassword.gridx = 1;
		gbc_btnChangePassword.gridy = 34;
		frameAdmin.getContentPane().add(btnChangePassword, gbc_btnChangePassword);

		JButton btnLogout = new JButton("\u2B05 Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameAdmin.dispose();
				Main.main(null);
			}
		});
		btnLogout.setMinimumSize(new Dimension(140, 30));
		btnLogout.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnLogout = new GridBagConstraints();
		gbc_btnLogout.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLogout.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogout.gridx = 3;
		gbc_btnLogout.gridy = 34;
		frameAdmin.getContentPane().add(btnLogout, gbc_btnLogout);
	}

}
