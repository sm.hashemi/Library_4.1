import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.Component;

import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;

import model.Library;
import model.Logger;

import javax.swing.SwingConstants;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private static JFrame frameMain;
	private static boolean loadedOnStart = false;

	/**
	 * 
	 * Launch the main application.
	 * 
	 * @author Seyed_Mahdi_Hashemi_Fesharaki
	 * @version 4.1
	 * 
	 */
	public static void main(String[] args) {
		InitializeFrameMain();
		frameMain.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void InitializeFrameMain() {
		if (!loadedOnStart) { //just run in start, load data & check log
			Library.loadData();
			Logger.logReader();
			loadedOnStart = true;
		}		
		frameMain = new JFrame();
		frameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMain.setResizable(false);
		frameMain.setTitle("Main");
		frameMain.setVisible(true);
		frameMain.setBounds(100, 100, 472, 358);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 100, 120, 100, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frameMain.getContentPane().setLayout(gridBagLayout);

		frameMain.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				Library.saveData();
			}
		});

		JLabel lblAllah = new JLabel("\u0628\u0650\u0633\u0652\u0645\u0650 \u0627\u0644\u0644\u064E\u0651\u0647\u0650 \u0627\u0644\u0631\u064E\u0651\u062D\u0652\u0645\u064E\u0646\u0650 \u0627\u0644\u0631\u064E\u0651\u062D\u0650\u064A\u0645\u0650");
		lblAllah.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAllah.setForeground(new Color(0, 0, 128));
		lblAllah.setFont(new Font("A Ordibehesht", Font.PLAIN, 20));
		lblAllah.setHorizontalAlignment(SwingConstants.CENTER);
		lblAllah.setPreferredSize(new Dimension(160, 33));
		lblAllah.setBorder(new LineBorder(new Color(51, 153, 255)));
		GridBagConstraints gbc_lblAllah = new GridBagConstraints();
		gbc_lblAllah.gridwidth = 3;
		gbc_lblAllah.insets = new Insets(0, 0, 5, 5);
		gbc_lblAllah.gridx = 1;
		gbc_lblAllah.gridy = 1;
		frameMain.getContentPane().add(lblAllah, gbc_lblAllah);

		JLabel lblWelcome = new JLabel("Welcome to Library Management 4.1");
		lblWelcome.setHorizontalTextPosition(SwingConstants.CENTER);
		lblWelcome.setForeground(new Color(0, 0, 128));
		lblWelcome.setBackground(new Color(0, 0, 255));
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setPreferredSize(new Dimension(176, 33));
		lblWelcome.setBorder(new LineBorder(SystemColor.textHighlight));
		lblWelcome.setFont(new Font("Supercell-Magic", Font.PLAIN, 11));
		GridBagConstraints gbc_lblWelcome = new GridBagConstraints();
		gbc_lblWelcome.gridwidth = 3;
		gbc_lblWelcome.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblWelcome.insets = new Insets(0, 0, 5, 5);
		gbc_lblWelcome.gridx = 1;
		gbc_lblWelcome.gridy = 2;
		frameMain.getContentPane().add(lblWelcome, gbc_lblWelcome);

		JButton btnAdminLogin = new JButton("Admin Login");
		btnAdminLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdminLogin.main(null);
			}
		});

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 3;
		frameMain.getContentPane().add(verticalStrut, gbc_verticalStrut);

		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_1.gridx = 1;
		gbc_verticalStrut_1.gridy = 4;
		frameMain.getContentPane().add(verticalStrut_1, gbc_verticalStrut_1);
		btnAdminLogin.setPreferredSize(new Dimension(300, 35));
		btnAdminLogin.setMaximumSize(new Dimension(149, 30));
		btnAdminLogin.setMinimumSize(new Dimension(155, 35));
		GridBagConstraints gbc_btnAdminLogin = new GridBagConstraints();
		gbc_btnAdminLogin.gridwidth = 3;
		gbc_btnAdminLogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAdminLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdminLogin.gridx = 1;
		gbc_btnAdminLogin.gridy = 5;
		frameMain.getContentPane().add(btnAdminLogin, gbc_btnAdminLogin);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 1;
		gbc_horizontalStrut_1.gridy = 6;
		frameMain.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 1;
		gbc_horizontalStrut_6.gridy = 7;
		frameMain.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		JButton btnMemberLogin = new JButton("Member Login");
		btnMemberLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Library.member.isEmpty()) {
					Library.alert("No member exist!", "Error!", true, true);
					return;
				}
				MemberLogin.main(null);
			}
		});
		btnMemberLogin.setMinimumSize(new Dimension(155, 35));
		btnMemberLogin.setPreferredSize(new Dimension(300, 35));
		GridBagConstraints gbc_btnMemberLogin = new GridBagConstraints();
		gbc_btnMemberLogin.gridwidth = 3;
		gbc_btnMemberLogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnMemberLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnMemberLogin.gridx = 1;
		gbc_btnMemberLogin.gridy = 8;
		frameMain.getContentPane().add(btnMemberLogin, gbc_btnMemberLogin);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 9;
		frameMain.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 10;
		frameMain.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Library.saveData();
			}
		});
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.fill = GridBagConstraints.BOTH;
		gbc_btnSave.insets = new Insets(0, 0, 5, 5);
		gbc_btnSave.gridx = 1;
		gbc_btnSave.gridy = 11;
		frameMain.getContentPane().add(btnSave, gbc_btnSave);

		JButton btnAbout = new JButton("About");
		GridBagConstraints gbc_btnAbout = new GridBagConstraints();
		gbc_btnAbout.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAbout.insets = new Insets(0, 0, 5, 5);
		gbc_btnAbout.gridx = 2;
		gbc_btnAbout.gridy = 11;
		frameMain.getContentPane().add(btnAbout, gbc_btnAbout);
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					About.main(null);
					// Desktop.getDesktop().edit(about);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnAbout.setMinimumSize(new Dimension(72, 28));
		btnAbout.setPreferredSize(new Dimension(100, 32));

		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Library.loadData();
			}
		});
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.fill = GridBagConstraints.BOTH;
		gbc_btnLoad.insets = new Insets(0, 0, 5, 5);
		gbc_btnLoad.gridx = 3;
		gbc_btnLoad.gridy = 11;
		frameMain.getContentPane().add(btnLoad, gbc_btnLoad);

		Box horizontalBox = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox = new GridBagConstraints();
		gbc_horizontalBox.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalBox.gridx = 1;
		gbc_horizontalBox.gridy = 12;
		frameMain.getContentPane().add(horizontalBox, gbc_horizontalBox);
	}

	/**
	 * Dispose the main frame.
	 */
	public static void disposeFrame() {
		frameMain.dispose();
	}
}
