import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.SwingConstants;

import model.Library;
import model.Member;

import java.awt.Window.Type;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MemberLogin {

	private static JFrame frmMemberLogin;
	private static JTextField textFieldId;
	private static JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeMemberLogin();
		frmMemberLogin.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeMemberLogin() {
		frmMemberLogin = new JFrame();
		frmMemberLogin.setResizable(false);
		frmMemberLogin.setType(Type.POPUP);
		frmMemberLogin.setTitle("Memer Login");
		frmMemberLogin.setBounds(100, 100, 330, 230);
		frmMemberLogin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 50, 0, 0, 94, 50, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMemberLogin.getContentPane().setLayout(gridBagLayout);

		JLabel lblMemberId = new JLabel("Member ID:");
		lblMemberId.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblMemberId = new GridBagConstraints();
		gbc_lblMemberId.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblMemberId.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberId.gridx = 1;
		gbc_lblMemberId.gridy = 1;
		frmMemberLogin.getContentPane().add(lblMemberId, gbc_lblMemberId);

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 2;
		gbc_verticalStrut.gridy = 1;
		frmMemberLogin.getContentPane().add(verticalStrut, gbc_verticalStrut);

		textFieldId = new JTextField();
		GridBagConstraints gbc_textFieldId = new GridBagConstraints();
		gbc_textFieldId.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldId.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldId.gridx = 3;
		gbc_textFieldId.gridy = 1;
		frmMemberLogin.getContentPane().add(textFieldId, gbc_textFieldId);
		textFieldId.setColumns(10);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 2;
		frmMemberLogin.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 1;
		gbc_horizontalStrut_1.gridy = 3;
		frmMemberLogin.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		JLabel lbMemberPassword = new JLabel("Password:");
		lbMemberPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lbMemberPassword = new GridBagConstraints();
		gbc_lbMemberPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbMemberPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lbMemberPassword.gridx = 1;
		gbc_lbMemberPassword.gridy = 4;
		frmMemberLogin.getContentPane().add(lbMemberPassword, gbc_lbMemberPassword);

		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 3;
		gbc_passwordField.gridy = 4;
		frmMemberLogin.getContentPane().add(passwordField, gbc_passwordField);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 3;
		gbc_horizontalStrut_4.gridy = 5;
		frmMemberLogin.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 3;
		gbc_horizontalStrut_5.gridy = 6;
		frmMemberLogin.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 3;
		gbc_horizontalStrut_6.gridy = 7;
		frmMemberLogin.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 3;
		gbc_horizontalStrut_3.gridy = 8;
		frmMemberLogin.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 3;
		gbc_horizontalStrut_2.gridy = 9;
		frmMemberLogin.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get data from fields
				String id = textFieldId.getText();
				String password = String.valueOf(passwordField.getPassword());
				// try to login
				try {
					int loginMemberId = Integer.parseInt(id);
					if (loginMemberId >= 1000 && loginMemberId <= Member.getIdIndex()) {
						for (int i = 0; Library.member.getObject(i) != null; i++) {
							Member m = (Member) Library.member.getObject(i);
							if (m.getId() == loginMemberId) {
								if (m.getPassword().equals(password)) {
									Main.disposeFrame();
									frmMemberLogin.dispose();
									MemberMenu.main(m);
									return;
								} else {
									Library.alert("Wrong password!", "Member Login Error", true, false);
									return;
								}
							}
						}
					} else
						Library.alert("Invalid ID range!", "Member Login Error", true, false);
				} catch (NumberFormatException e) {
					Library.alert("Invalid ID number!", "Member Login Error", true, false);
					return;
				}

			}
		});
		btnLogin.setPreferredSize(new Dimension(70, 25));
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.gridwidth = 3;
		gbc_btnLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogin.gridx = 1;
		gbc_btnLogin.gridy = 10;
		frmMemberLogin.getContentPane().add(btnLogin, gbc_btnLogin);
	}
}
