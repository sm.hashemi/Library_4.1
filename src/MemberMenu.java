import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.border.LineBorder;

import model.Book;
import model.Library;
import model.Member;
import options.BookSearch;
import options.SelfMemberInfo;
import options.SelfMemberPass;

import java.awt.Color;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MemberMenu {

	private static JFrame frameMember;
	private static Member loggedInMember;

	/**
	 * launch the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public static void main(Member m) {
		loggedInMember = m;
		initializeFrameMemberMenu();
		frameMember.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameMemberMenu() {
		frameMember = new JFrame();
		frameMember.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\MahdiYar\\Downloads\\Compressed\\led-icons\\user_business_boss.png"));
		frameMember.setResizable(false);
		frameMember.setTitle("Member");
		frameMember.setVisible(true);
		frameMember.setBounds(100, 100, 472, 333);
		frameMember.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 50, 0, 0, 0, 50, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		frameMember.getContentPane().setLayout(gridBagLayout);

		JLabel lblNewLabel_2 = new JLabel("  Hello " + loggedInMember.getName() + " " + loggedInMember.getFamily() + "!");
		lblNewLabel_2.setForeground(new Color(0, 153, 0));
		lblNewLabel_2.setMaximumSize(new Dimension(140, 14));
		lblNewLabel_2.setMinimumSize(new Dimension(323, 22));
		lblNewLabel_2.setPreferredSize(new Dimension(140, 18));
		lblNewLabel_2.setBorder(new LineBorder(new Color(0, 204, 0)));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_2.gridwidth = 3;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 1;
		frameMember.getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);

		String nowDate = new SimpleDateFormat("EEE yyyy-MM-dd HH:mm").format(new Date());
		JLabel lblNewLabel_3 = new JLabel("  Today " + nowDate);
		lblNewLabel_3.setForeground(new Color(0, 153, 0));
		lblNewLabel_3.setBorder(new LineBorder(new Color(0, 204, 0)));
		lblNewLabel_3.setMinimumSize(new Dimension(323, 22));
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_3.gridwidth = 3;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 1;
		gbc_lblNewLabel_3.gridy = 2;
		frameMember.getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);
		if (loggedInMember.isBorrower()) {
			if (loggedInMember.isDelayer()) {
				JLabel lblNewLabel_4 = new JLabel("  WARNING! You must return the Book!   Penalty: $" + loggedInMember.getPenalty());
				lblNewLabel_4.setForeground(new Color(255, 0, 0));
				lblNewLabel_4.setBorder(new LineBorder(new Color(255, 69, 0)));
				lblNewLabel_4.setMinimumSize(new Dimension(323, 22));
				GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
				gbc_lblNewLabel_4.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblNewLabel_4.gridwidth = 3;
				gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_4.gridx = 1;
				gbc_lblNewLabel_4.gridy = 3;
				frameMember.getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
				
				Book b=(Book) Library.book.searchById(loggedInMember.getBorrowedBookId());
				JLabel lblNewLabel_5 = new JLabel(" Book: " + b.getName() + "   Borrow: 20" + loggedInMember.getBorrowDateString() + "   Return: 20" + loggedInMember.getReturnDateString());
				lblNewLabel_5.setForeground(new Color(255, 0, 0));
				lblNewLabel_5.setBorder(new LineBorder(new Color(255, 69, 0)));
				lblNewLabel_5.setMinimumSize(new Dimension(323, 22));
				GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
				gbc_lblNewLabel_5.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblNewLabel_5.gridwidth = 3;
				gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_5.gridx = 1;
				gbc_lblNewLabel_5.gridy = 4;
				frameMember.getContentPane().add(lblNewLabel_5, gbc_lblNewLabel_5);
			} else {
				JLabel lblNewLabel_4 = new JLabel("  Keep Reading! you have " + loggedInMember.getDaysRemainString() + " Days to returning time!");
				lblNewLabel_4.setForeground(new Color(0, 153, 0));
				lblNewLabel_4.setBorder(new LineBorder(new Color(0, 204, 0)));
				lblNewLabel_4.setMinimumSize(new Dimension(323, 22));
				GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
				gbc_lblNewLabel_4.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblNewLabel_4.gridwidth = 3;
				gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_4.gridx = 1;
				gbc_lblNewLabel_4.gridy = 3;
				frameMember.getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
				
				Book b=(Book) Library.book.searchById(loggedInMember.getBorrowedBookId());
				JLabel lblNewLabel_5 = new JLabel(" Book: " + b.getName() + "  Borrow: 20" + loggedInMember.getBorrowDateString() + "  Return: 20" + loggedInMember.getReturnDateString());
				lblNewLabel_5.setForeground(new Color(255, 0, 0));
				lblNewLabel_5.setBorder(new LineBorder(new Color(0, 204, 0)));
				lblNewLabel_5.setMinimumSize(new Dimension(323, 22));
				GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
				gbc_lblNewLabel_5.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblNewLabel_5.gridwidth = 3;
				gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_5.gridx = 1;
				gbc_lblNewLabel_5.gridy = 4;
				frameMember.getContentPane().add(lblNewLabel_5, gbc_lblNewLabel_5);
			}
		} else {
			JLabel lblNewLabel_4 = new JLabel("  You don't borrowed book! ِDon't Like Reading?");
			lblNewLabel_4.setForeground(new Color(0, 153, 0));
			lblNewLabel_4.setBorder(new LineBorder(new Color(0, 204, 0)));
			lblNewLabel_4.setMinimumSize(new Dimension(323, 22));
			GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
			gbc_lblNewLabel_4.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblNewLabel_4.gridwidth = 3;
			gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel_4.gridx = 1;
			gbc_lblNewLabel_4.gridy = 3;
			frameMember.getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
		}

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 3;
		frameMember.getContentPane().add(verticalStrut, gbc_verticalStrut);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_2.gridx = 1;
		gbc_verticalStrut_2.gridy = 4;
		frameMember.getContentPane().add(verticalStrut_2, gbc_verticalStrut_2);

		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_1.gridx = 1;
		gbc_verticalStrut_1.gridy = 5;
		frameMember.getContentPane().add(verticalStrut_1, gbc_verticalStrut_1);

		JButton btnSearchBook = new JButton("\uD83D\uDD0E  Search Book");
		btnSearchBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookSearch.main(null);
			}
		});
		btnSearchBook.setMinimumSize(new Dimension(140, 30));
		btnSearchBook.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnSearchBook = new GridBagConstraints();
		gbc_btnSearchBook.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSearchBook.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearchBook.gridx = 1;
		gbc_btnSearchBook.gridy = 6;
		frameMember.getContentPane().add(btnSearchBook, gbc_btnSearchBook);

		JButton btnUpdateMember = new JButton("\uD83D\uDD03  Update Info");
		btnUpdateMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SelfMemberInfo.main(loggedInMember);
			}
		});
		btnUpdateMember.setMinimumSize(new Dimension(140, 30));
		btnUpdateMember.setPreferredSize(new Dimension(150, 30));
		GridBagConstraints gbc_btnUpdateMember = new GridBagConstraints();
		gbc_btnUpdateMember.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUpdateMember.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdateMember.gridx = 3;
		gbc_btnUpdateMember.gridy = 6;
		frameMember.getContentPane().add(btnUpdateMember, gbc_btnUpdateMember);

		JLabel lblNewLabel_1 = new JLabel("_________________________________________________");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridwidth = 3;
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 7;
		frameMember.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		Component horizontalStrut_24 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_24 = new GridBagConstraints();
		gbc_horizontalStrut_24.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_24.gridx = 1;
		gbc_horizontalStrut_24.gridy = 8;
		frameMember.getContentPane().add(horizontalStrut_24, gbc_horizontalStrut_24);

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 1;
		gbc_horizontalStrut_7.gridy = 9;
		frameMember.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JButton btnChangePassword = new JButton("\uD83D\uDD13  Change Password");
		btnChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SelfMemberPass.main(loggedInMember);
			}
		});
		btnChangePassword.setPreferredSize(new Dimension(140, 30));
		btnChangePassword.setMinimumSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnChangePassword = new GridBagConstraints();
		gbc_btnChangePassword.insets = new Insets(0, 0, 5, 5);
		gbc_btnChangePassword.gridx = 1;
		gbc_btnChangePassword.gridy = 10;
		frameMember.getContentPane().add(btnChangePassword, gbc_btnChangePassword);

		JButton btnLogout = new JButton("\u2B05 Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameMember.dispose();
				Main.main(null);
			}
		});
		btnLogout.setMinimumSize(new Dimension(140, 30));
		btnLogout.setPreferredSize(new Dimension(140, 30));
		GridBagConstraints gbc_btnLogout = new GridBagConstraints();
		gbc_btnLogout.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLogout.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogout.gridx = 3;
		gbc_btnLogout.gridy = 10;
		frameMember.getContentPane().add(btnLogout, gbc_btnLogout);
	}

}
