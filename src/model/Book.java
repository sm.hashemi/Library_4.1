package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class Book implements Listable, Serializable {
	// State :
	private String name;
	private String author;
	private String category;
	private byte publishYear;
	private boolean isBorrowed;
	private int borrowerId;
	private int id;
	private LocalDate borrowDate;
	private static int idIndex = 100; // just ++

	// Constructor :
	public Book(String name, String author, byte year, String category) {
		this.setName(name);
		this.setAuthor(author);
		this.setCategory(category);
		this.setPubYear(year);
		this.setId();
		JOptionPane.showMessageDialog(null, "     Book ID : " + this.getId(), "✔ Registered Successfully!", JOptionPane.INFORMATION_MESSAGE);
		idIndex++;
	}

	public Book() {
	}

	// Methods :
	public static int getIdIndex() {
		return idIndex;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public void setPubYear(byte year) {
		this.publishYear = year;
	}

	public byte getPubYear() {
		return publishYear;
	}

	public String getPubYearString() {
		if (publishYear < 10)
			return "0" + String.valueOf(publishYear);
		else
			return String.valueOf(publishYear);

	}

	public void setId() { // just use in create
		this.id = idIndex;
	}

	public int getId() {
		return id;
	}

	public void setAsBorrowed() {
		this.isBorrowed = true;
	}

	public void setAsReturned() {
		this.isBorrowed = false;
	}

	public boolean isBorrowed() {
		return isBorrowed;
	}

	public void setBorrowerId(int borrower) {
		this.borrowerId = borrower;
	}

	public int getBorrowerId() {
		return borrowerId;
	}

	public boolean isDelayed() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, borrowDate);
		if (remain > 10)
			return true;
		else
			return false;
	}

	public int getDaysRemain() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, borrowDate);
		return remain;
	}

	public String getDaysRemainString() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, borrowDate);
		return String.valueOf(remain);
	}

	public void setBorrowDate(int y, int m, int d) {
		this.borrowDate = LocalDate.parse(y + "-" + m + "-" + d, DateTimeFormatter.ofPattern("y-M-d"));
	}

	public void setBorrowDateAsToday() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		this.borrowDate = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
	}

	public LocalDate getBorrowDate() {
		return borrowDate;
	}

	public String getBorrowDateString() {
		String date = borrowDate.format(DateTimeFormatter.ofPattern("yy-MM-dd"));
		return "13" + date;
	}

}
