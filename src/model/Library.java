package model;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class Library {
	public static Scanner scanner = new Scanner(System.in);
	public static LinkedList book = new LinkedList();
	public static LinkedList member = new LinkedList();
	private static String adminPassword = ""; // fix later = save it on file

	// Methods:
	public static String getAdminPassword() {
		return adminPassword;
	}

	public static void setAdminPassword(String adminPasswordNew) {
		adminPassword = adminPasswordNew;
	}

	// ___________________Save/Load Data___________________
	
	public static void saveData(){ // & clear log
		try {
			FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + "\\Desktop\\Mdata.lib");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			for (int i = 0; i != member.getSize(); i++)
				oos.writeObject((Member) member.getObject(i));

			fos.close();
			Library.alert("✔  Members data saved Successfully!", "Done!", true, false);
		} catch (Exception e) { // catches any exception
			e.printStackTrace();
			alert("✖  Could not save Members data in file!", "Error!", true, true);
		}
		try {
			FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + "\\Desktop\\Bdata.lib"); // %UserProfile%\\Desktop\\Bdata.lib
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			for (int i = 0; i != book.getSize(); i++)
				oos.writeObject((Book) book.getObject(i));

			fos.close();
			Library.alert("✔  Books data saved Successfully!", "Done!", true, false);
		} catch (Exception e) { // catches any exception
			e.printStackTrace();
			alert("✖  Could not save Books data in file!", "Error!", true, true);
		}
		try {
			FileWriter logCleaner = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log");
			logCleaner.write("");
			logCleaner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void loadData() {
		try {
			member.resetLinkedList();
			FileInputStream fis = new FileInputStream(System.getProperty("user.home") + "\\Desktop\\Mdata.lib");
			ObjectInputStream ois = new ObjectInputStream(fis);

			try {
				while (true)
					member.add((Member) ois.readObject());
			} catch (EOFException e) {
				Library.alert("✔  Members data loaded Successfully!", "Done!", true, false);
			}

			fis.close();
		} catch (FileNotFoundException e) {
			alert("✖  Members data file not found!", "Error!", true, true);
		} catch (Exception e) { // catch any exception
			e.printStackTrace();
			alert("✖  Could not load Members data from file!", "Error!", true, true);
		}
		try {
			book.resetLinkedList();
			FileInputStream fis = new FileInputStream(System.getProperty("user.home") + "\\Desktop\\Bdata.lib");
			ObjectInputStream ois = new ObjectInputStream(fis);

			try {
				while (true)
					book.add((Book) ois.readObject());
			} catch (EOFException e) {
				Library.alert("✔  Books data loaded Successfully!", "Done!", true, false);
			}

			fis.close();
		} catch (FileNotFoundException e) {
			alert("✖  Books data file not found!", "Error!", true, true);
		} catch (Exception e) { // catch any exception
			e.printStackTrace();
			alert("✖  Could not load Books data from file!", "Error!", true, true);
		}
	}

	// ___________________Utils___________________

	public static void alert(String message, String title, boolean autohide, boolean beep) {
		if (beep)
			Toolkit.getDefaultToolkit().beep();
		JOptionPane optionPane = new JOptionPane(message, JOptionPane.WARNING_MESSAGE);
		JDialog dialog = optionPane.createDialog(title);
		if (autohide) {
			Timer timer = new Timer(2000, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dialog.setVisible(false);
					dialog.dispose();
				}
			});
			timer.start();
		}
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}

	public static boolean selectOption(String message, boolean beep) {
		if (beep)
			Toolkit.getDefaultToolkit().beep();
		JLabel labels = new JLabel(message);
		labels.add(new JLabel("User Name", SwingConstants.RIGHT));
		labels.add(new JLabel("Password", SwingConstants.RIGHT));
		int replay = JOptionPane.showConfirmDialog(null, message, "Select an Option", JOptionPane.YES_NO_OPTION);
		if (replay == JOptionPane.YES_OPTION)
			return true;
		else
			return false;
	}

	public static byte convertMonthToNum(String month) {
		if (month.equals(""))
			return 0;
		else if (month.equals("Farvardin"))
			return 1;
		else if (month.equals("Ordibehesht"))
			return 2;
		else if (month.equals("Khordad"))
			return 3;
		else if (month.equals("Tir"))
			return 4;
		else if (month.equals("Mordad"))
			return 5;
		else if (month.equals("Shahrivar"))
			return 6;
		else if (month.equals("Mehr"))
			return 7;
		else if (month.equals("Aban"))
			return 8;
		else if (month.equals("Azar"))
			return 9;
		else if (month.equals("Dey"))
			return 10;
		else if (month.equals("Bahman"))
			return 11;
		else if (month.equals("Esfand"))
			return 12;
		return 0;
	}

	public static String convertNumToMonth(int num) {
		if (num == 0)
			return "";
		else if (num == 1)
			return "Farvardin";
		else if (num == 2)
			return "Ordibehesht";
		else if (num == 3)
			return "Khordad";
		else if (num == 4)
			return "Tir";
		else if (num == 5)
			return "Mordad";
		else if (num == 6)
			return "Shahrivar";
		else if (num == 7)
			return "Mehr";
		else if (num == 8)
			return "Aban";
		else if (num == 9)
			return "Azar";
		else if (num == 10)
			return "Dey";
		else if (num == 11)
			return "Bahman";
		else if (num == 12)
			return "Esfand";
		return "";
	}
}
