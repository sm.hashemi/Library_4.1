package model;

public class LinkedList {
	// States
	private Node first = null;
	private Node last = null;

	// Methods
	public void add(Listable l) {
		Node node = new Node(l, null);
		if (first == null) { // first null check in other methods removed
								// because it check in higher levels
			first = last = node;
		} else {
			last.setNext(node);
			last = node;
		}
	}

	public void remove(Listable obj) {
		if (first.getObj().equals(obj)) { // its first
			first = first.getNext();
			return;
		} else if (last.getObj().equals(obj)) { // its last
			Node p = first;
			while (p.getNext() != last)
				p = p.getNext();
			p.setNext(null);
			last = p;
			return;
		} else { // its middle
			Node p = first;
			while (p.getNext() != null) {
				if ((p.getNext().getObj().equals(obj))) {
					p.setNext(p.getNext().getNext());
					;
					return;
				}
				p = p.getNext();
				return;
			}
		}
	}

	public Object searchById(int id) {
		Node p = first;
		while (p != null) {
			if ((p.getObj().getId() == id)) {
				return p.getObj();
			}
			p = p.getNext();
		}
		return null;
	}

	public Object getObject(int num) {
		Node p = first;
		int i = 0;
		while (p != null) {
			if (i == num) {
				return p.getObj();
			}
			p = p.getNext();
			i++;
		}
		return null;
	}

	public boolean isEmpty() {
		if (first == null)
			return true;
		else
			return false;
	}

	public int getSize() {
		Node p = first;
		int i = 0;
		while (p != null) {
			p = p.getNext();
			i++;
		}
		return i;
	}

	public void resetLinkedList() {
		first = last = null;
	}
}
