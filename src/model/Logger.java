package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Logger {
	public static FileWriter logWriter;
	public static Scanner logReader;

	public static void logBokAdd(Book b) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			if (b.getCategory().equals(""))
				logWriter.write(getDate() + "  bokAdd  :  " + b.getName() + " " + b.getAuthor() + " " + b.getPubYearString() + " no");
			else
				logWriter.write(getDate() + "  bokAdd  :  " + b.getName() + " " + b.getAuthor() + " " + b.getPubYearString() + " " + b.getCategory());
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logBokUpd(Book b) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			if (b.getCategory().equals(""))
				logWriter.write(getDate() + "  bokUpd  :  " + b.getId() + " " + b.getName() + " " + b.getAuthor() + " " + b.getPubYearString() + " no");
			else
				logWriter.write(getDate() + "  bokUpd  :  " + b.getId() + " " + b.getName() + " " + b.getAuthor() + " " + b.getPubYearString() + " " + b.getCategory());
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logBokDel(int id) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  bokDel  :  " + id);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logMemAdd(Member m) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  memAdd  :  " + m.getName() + " " + m.getFamily() + " " + m.getBirthString() + " " + m.getGender());
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logMemUpd(Member m) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  memUpd  :  " + m.getId() + " " + m.getName() + " " + m.getFamily() + " " + m.getBirthString() + " " + m.getGender());
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logMemDel(int id) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  memDel  :  " + id);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logBorrow(int idm, int idb) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  borrow  :  " + idm + " " + idb);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logReturn(int idm, int idb) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  return  :  " + idm + " " + idb);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logAdminP(String ps) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  adminP  :  " + ps);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logSelfPs(int id, String ps) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  selfPs  :  " + id + " " + ps);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void logSelfIn(int id, String ps) {
		try {
			logWriter = new FileWriter(System.getProperty("user.home") + "\\Desktop\\lib.log", true);
			logWriter.write(getDate() + "  selfIn  :  " + id + " " + ps);
			logWriter.write(String.format("%n"));
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void logReader() {
		try {
			logReader = new Scanner(new File(System.getProperty("user.home") + "\\Desktop\\lib.log"));
			while (logReader.hasNext()) { // has any log of not saved task ?
				String date = logReader.next(); // skip date (save for borrow)
				logReader.next(); // skip time
				switch (logReader.next()) {
				case "bokAdd": { // 100
					logReader.next(); // skip ":"
					// get data
					String name = logReader.next();
					String author = logReader.next();
					Byte year = logReader.nextByte();
					String category = logReader.next();
					if (category.equals("no"))
						category = "";
					// new & add
					Book b = new Book(name, author, year, category);
					Library.book.add(b);
					continue;
				}
				case "bokUpd": { // 100
					logReader.next(); // skip ":"
					Book b = (Book) Library.book.searchById(logReader.nextInt());
					b.setName(logReader.next());
					b.setAuthor(logReader.next());
					b.setPubYear(logReader.nextByte());
					String category = logReader.next();
					if (category.equals("no"))
						category = "";
					b.setCategory(category);
					continue;
				}
				case "bokDel": { // 100
					logReader.next(); // skip ":"
					Library.book.remove((Listable) Library.book.searchById(logReader.nextInt()));
					continue;
				}
				case "memAdd": { // 100
					logReader.next(); // skip ":"
					String name = logReader.next();
					String family = logReader.next();
					LocalDate birth = LocalDate.parse(logReader.next(), DateTimeFormatter.ofPattern("y-M-d"));
					Byte year = (byte) birth.getYear();
					Byte month = (byte) birth.getMonthValue();
					Byte day = (byte) birth.getDayOfMonth();
					boolean gender = logReader.nextBoolean();
					Member m = new Member(name, family, year, month, day, gender);
					Library.member.add(m);
					continue;
				}
				case "memUpd": { // 100
					logReader.next(); // skip ":"
					Member m = (Member) Library.member.searchById(logReader.nextInt());
					m.setName(logReader.next());
					m.setFamily(logReader.next());
					LocalDate birth = LocalDate.parse(logReader.next(), DateTimeFormatter.ofPattern("y-M-d"));
					m.setBirth((byte) birth.getYear(), (byte) birth.getMonthValue(), (byte) birth.getDayOfMonth());
					m.setGender(logReader.nextBoolean());
					continue;
				}
				case "memDel": { // 100
					logReader.next(); // skip ":"
					Library.member.remove((Listable) Library.member.searchById(logReader.nextInt()));
					continue;
				}
				case "borrow": { // 100
					logReader.next(); // skip ":"
					Member m = (Member) Library.member.searchById(logReader.nextInt());
					Book b = (Book) Library.book.searchById(logReader.nextInt());
					LocalDate borrowDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("y-M-d"));
					m.setAsBorrower();
					m.setBorrowedBookId(b.getId());
					m.setBorrowDate(borrowDate.getYear(), borrowDate.getMonthValue(), borrowDate.getDayOfMonth());
					b.setAsBorrowed();
					b.setBorrowerId(m.getId());
					b.setBorrowDate(borrowDate.getYear(), borrowDate.getMonthValue(), borrowDate.getDayOfMonth());
					continue;
				}
				case "return": { // 100
					logReader.next(); // skip ":"
					Member m = (Member) Library.member.searchById(logReader.nextInt());
					Book b = (Book) Library.book.searchById(logReader.nextInt());
					m.setAsReturner();
					m.setBorrowedBookId(0);
					b.setAsReturned();
					b.setBorrowerId(0);
					continue;
				}
				case "adminP": { // 100
					logReader.next(); // skip ":"
					Library.setAdminPassword(logReader.next());
					continue;
				}
				case "selfPs": { // 100
					logReader.next(); // skip ":"
					Member m = (Member) Library.member.searchById(logReader.nextInt());
					m.setPassword(logReader.next());
				}
				}
			}
		} catch (FileNotFoundException e1) {
		}
	}

	public static String getDate() {
		String nowDate = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date());
		return nowDate;
	}

}
