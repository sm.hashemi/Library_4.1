package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class Member extends Person implements Listable, Serializable {
	// State :
	private boolean isBorrower;
	private int borrowedBookId;
	private LocalDate borrowDate;
	private LocalDate returnDate;
	private int id;
	private String password;
	private static int idIndex = 1000; // just ++
	private int penalty;

	// Constructor:
	public Member(String name, String family, byte year, byte month, byte day, boolean gender) {
		this.setName(name);
		this.setFamily(family);
		this.setBirth(year, month, day);
		this.setGender(gender);
		this.setId();
		this.setDefaultPassword();
		JOptionPane.showMessageDialog(null, "     MemberID : " + this.getId() + "\n     Password : " + this.getPassword(), "✔ Sign-up Successfull!", JOptionPane.INFORMATION_MESSAGE);
		idIndex++;
	}

	public Member() {
	}

	// interface :
	public static int getIdIndex() {
		return idIndex;
	}

	public void setAsBorrower() {
		this.isBorrower = true;
	}

	public void setAsReturner() {
		this.isBorrower = false;
		this.penalty = 0;
	}

	public void setBorrowedBookId(int bookId) {
		this.borrowedBookId = bookId;
	}

	public int getBorrowedBookId() {
		return borrowedBookId;
	}

	public boolean isBorrower() {
		return isBorrower;
	}

	public void setId() { // just use in create
		this.id = idIndex;
	}

	public int getId() {
		return id;
	}

	public void setDefaultPassword() {
		password = String.valueOf(id);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setBorrowDateAsToday() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		this.borrowDate = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		this.returnDate = borrowDate.plusDays(10);
	}

	public boolean isDelayer() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, borrowDate);
		if (remain <= 10&&remain>=0)
			return false;
		else
			return true;
	}

	public int getDaysRemain() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, returnDate);
		return remain;
	}

	public String getDaysRemainString() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int remain = (int) ChronoUnit.DAYS.between(now, returnDate);
		return String.valueOf(remain);
	}

	public void setBorrowDate(int y, int m, int d) {
		this.borrowDate = LocalDate.parse(y + "-" + m + "-" + d, DateTimeFormatter.ofPattern("y-M-d"));
		this.returnDate = borrowDate.plusDays(10);
	}

	public LocalDate getBorrowDate() {
		return borrowDate;
	}

	public String getBorrowDateString() {
		String date = borrowDate.format(DateTimeFormatter.ofPattern("yy-MM-dd"));
		return date;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public String getReturnDateString() {
		String date = returnDate.format(DateTimeFormatter.ofPattern("yy-MM-dd"));
		return date;
	}

	public int getPenalty() {
		String nowDate = new SimpleDateFormat("y-M-d").format(new Date());
		LocalDate now = LocalDate.parse(nowDate, DateTimeFormatter.ofPattern("y-M-d"));
		int expired = (int) ChronoUnit.DAYS.between(returnDate, now);
		penalty = expired * 1000;
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
}