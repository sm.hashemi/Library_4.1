package model;

public class Node {
	// States
	private Node next = null;
	private Listable obj;

	// Constructor
	public Node(Listable obj, Node next) {
		this.obj = obj;
		this.next = next;
	}

	// Methods
	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	public Listable getObj() {
		return obj;
	}
}