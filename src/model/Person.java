package model;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@SuppressWarnings("serial")
public class Person implements Serializable {
	// state :
	protected String name;
	protected String family;
	protected boolean gender;
	protected LocalDate birth;

	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getFamily() {
		return family;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public boolean getGender() {
		return gender;
	}

	public String getGenderString() {
		if (gender == true)
			return "Male";
		else
			return "Female";
	}

	public void setBirth(int y, int m, int d) {
		this.birth = LocalDate.parse(y + "-" + m + "-" + d, DateTimeFormatter.ofPattern("y-M-d"));
	}

	public byte getBirthYear() {
		return (byte) birth.getYear();
	}

	public byte getBirthMonth() {
		return (byte) birth.getMonthValue();
	}

	public byte getBirthDay() {
		return (byte) birth.getDayOfMonth();
	}

	public LocalDate getBirth() {
		return birth;
	}

	public String getBirthString() {
		String date = birth.format(DateTimeFormatter.ofPattern("yy-MM-dd"));
		return date;
	}

}