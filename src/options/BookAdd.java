package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.Book;
import model.Library;
import model.Logger;

import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class BookAdd {

	private static JFrame frameAddBook;
	private static JTextField fieldAuthor;
	private static JTextField fieldName;
	private static JSpinner spinnerYear;
	private static JComboBox<?> comboboxCategory;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		InitializeFrameAddBook();
		frameAddBook.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void InitializeFrameAddBook() {
		frameAddBook = new JFrame();
		frameAddBook.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frameAddBook.setTitle("Add Book");
		frameAddBook.setVisible(true);
		frameAddBook.setBounds(100, 100, 381, 373);
		frameAddBook.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 20, 36, 200, 20 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 25, 0, 0, 20, 0, -8, 25, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0 };
		frameAddBook.getContentPane().setLayout(gridBagLayout);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 2;
		gbc_horizontalStrut_10.gridy = 0;
		frameAddBook.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		JLabel lblName = new JLabel("Name :");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		frameAddBook.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 2;
		gbc_fieldName.gridy = 1;
		frameAddBook.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 2;
		frameAddBook.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 3;
		frameAddBook.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblAuthor = new JLabel("Author :");

		GridBagConstraints gbc_lblAuthor = new GridBagConstraints();
		gbc_lblAuthor.anchor = GridBagConstraints.EAST;
		gbc_lblAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_lblAuthor.gridx = 1;
		gbc_lblAuthor.gridy = 4;
		frameAddBook.getContentPane().add(lblAuthor, gbc_lblAuthor);

		fieldAuthor = new JTextField();
		GridBagConstraints gbc_fieldAuthor = new GridBagConstraints();
		gbc_fieldAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_fieldAuthor.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldAuthor.gridx = 2;
		gbc_fieldAuthor.gridy = 4;
		frameAddBook.getContentPane().add(fieldAuthor, gbc_fieldAuthor);
		fieldAuthor.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 5;
		frameAddBook.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 6;
		frameAddBook.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		JLabel lblAge = new JLabel("PubYear :");

		GridBagConstraints gbc_lblAge = new GridBagConstraints();
		gbc_lblAge.anchor = GridBagConstraints.EAST;
		gbc_lblAge.insets = new Insets(0, 0, 5, 5);
		gbc_lblAge.gridx = 1;
		gbc_lblAge.gridy = 7;
		frameAddBook.getContentPane().add(lblAge, gbc_lblAge);

		// -----------Panel for PubYear spinner------------

		JPanel panelPubYear = new JPanel();
		GridBagConstraints gbc_panelPubYear = new GridBagConstraints();
		gbc_panelPubYear.anchor = GridBagConstraints.WEST;
		gbc_panelPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_panelPubYear.gridx = 2;
		gbc_panelPubYear.gridy = 7;
		frameAddBook.getContentPane().add(panelPubYear, gbc_panelPubYear);

		JLabel label = new JLabel("13");
		panelPubYear.add(label);

		spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		panelPubYear.add(spinnerYear);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 1;
		gbc_horizontalStrut_5.gridy = 8;
		frameAddBook.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 2;
		gbc_horizontalStrut_7.gridy = 9;
		frameAddBook.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JLabel lblCategory = new JLabel("Category :");

		GridBagConstraints gbc_lblCategory = new GridBagConstraints();
		gbc_lblCategory.anchor = GridBagConstraints.EAST;
		gbc_lblCategory.insets = new Insets(0, 0, 5, 5);
		gbc_lblCategory.gridx = 1;
		gbc_lblCategory.gridy = 10;
		frameAddBook.getContentPane().add(lblCategory, gbc_lblCategory);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 10;
		frameAddBook.getContentPane().add(scrollPane, gbc_scrollPane);

		comboboxCategory = new JComboBox();
		comboboxCategory.setMaximumRowCount(12);
		comboboxCategory.setModel(new DefaultComboBoxModel(
				new String[] { "", "Sciences", "Computer", "Religion", "Refrence", "Date", "Story" }));
		scrollPane.setViewportView(comboboxCategory);

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 11;
		frameAddBook.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 12;
		frameAddBook.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 13;
		frameAddBook.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Add Button_________________

		JButton btnAdd = new JButton("Add Book!");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get data from fields
				String name = fieldName.getText();
				String author = fieldAuthor.getText();
				byte year = (byte) spinnerYear.getValue();
				String category = (String) comboboxCategory.getSelectedItem();
				// validate
				if (name.equals("")) {
					Library.alert("Please enter \"Name\"!", "Add Book Error!", true, true);
					return;
				}
				if (author.equals("")) {
					Library.alert("Please enter \"Author\"!", "Add Book Error!", true, true);
					return;
				}
				if (year == 0) {
					Library.alert("Please select \"Publish Year\"!", "Add Book Error!", true, true);
					return;
				}
				// new
				Book b = new Book(name, author, year, category);
				Library.book.add(b);
				Logger.logBokAdd(b);
				resetForm();
				return;
			}
		});

		btnAdd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 0, 5);
		gbc_btnAdd.gridwidth = 2;
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 14;
		frameAddBook.getContentPane().add(btnAdd, gbc_btnAdd);
	}

	/**
	 * Reset contents of the frame.
	 */
	public static void resetForm() {
		fieldName.setText("");
		fieldAuthor.setText("");
		spinnerYear.setValue((byte) 0);
		comboboxCategory.setSelectedItem("");
	}
}
