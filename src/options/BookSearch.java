﻿package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

public class BookSearch {

	private static JFrame frameSearch;
	private static JTextField fieldAuthor;
	private static JTextField fieldName;
	private static JComboBox<?> comboboxCategory;
	private static JTextField fieldId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameSearchBook();
		frameSearch.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void initializeFrameSearchBook() {
		frameSearch = new JFrame();
		frameSearch.setTitle("Advanced Book Search");
		frameSearch.setVisible(true);
		frameSearch.setBounds(100, 100, 380, 420);
		frameSearch.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 16, 36, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 20, -8, 25, 0, 0,
				0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frameSearch.getContentPane().setLayout(gridBagLayout);

		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_14 = new GridBagConstraints();
		gbc_horizontalStrut_14.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_14.gridx = 3;
		gbc_horizontalStrut_14.gridy = 0;
		frameSearch.getContentPane().add(horizontalStrut_14, gbc_horizontalStrut_14);

		JLabel topLine = new JLabel("-------- Advane Search is Here! --------");
		GridBagConstraints gbc_topLine = new GridBagConstraints();
		gbc_topLine.gridwidth = 3;
		gbc_topLine.insets = new Insets(0, 0, 5, 5);
		gbc_topLine.gridx = 1;
		gbc_topLine.gridy = 1;
		frameSearch.getContentPane().add(topLine, gbc_topLine);

		JLabel topLine2 = new JLabel("* Use one or More option! ");
		GridBagConstraints gbc_topLine2 = new GridBagConstraints();
		gbc_topLine2.gridwidth = 3;
		gbc_topLine2.insets = new Insets(0, 0, 5, 5);
		gbc_topLine2.gridx = 1;
		gbc_topLine2.gridy = 2;
		frameSearch.getContentPane().add(topLine2, gbc_topLine2);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 3;
		gbc_horizontalStrut_5.gridy = 3;
		frameSearch.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_13 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_13 = new GridBagConstraints();
		gbc_horizontalStrut_13.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_13.gridx = 3;
		gbc_horizontalStrut_13.gridy = 4;
		frameSearch.getContentPane().add(horizontalStrut_13, gbc_horizontalStrut_13);

		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_15 = new GridBagConstraints();
		gbc_horizontalStrut_15.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_15.gridx = 3;
		gbc_horizontalStrut_15.gridy = 5;
		frameSearch.getContentPane().add(horizontalStrut_15, gbc_horizontalStrut_15);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 3;
		gbc_horizontalStrut_11.gridy = 6;
		frameSearch.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		JCheckBox checkboxId = new JCheckBox("");
		checkboxId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxId = new GridBagConstraints();
		gbc_checkboxId.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxId.gridx = 1;
		gbc_checkboxId.gridy = 7;
		frameSearch.getContentPane().add(checkboxId, gbc_checkboxId);

		JLabel lblId = new JLabel("Book ID   :");
		lblId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.anchor = GridBagConstraints.WEST;
		gbc_lblId.insets = new Insets(0, 0, 5, 5);
		gbc_lblId.gridx = 2;
		gbc_lblId.gridy = 7;
		frameSearch.getContentPane().add(lblId, gbc_lblId);

		fieldId = new JTextField();
		fieldId.setEnabled(false);
		;
		GridBagConstraints gbc_fieldId = new GridBagConstraints();
		gbc_fieldId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldId.gridx = 3;
		gbc_fieldId.gridy = 7;
		frameSearch.getContentPane().add(fieldId, gbc_fieldId);
		fieldId.setColumns(10);

		// _______________________+________________________
		// ________________________________________________

		JLabel label_3 = new JLabel("_____________________________________________");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridwidth = 3;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 8;
		frameSearch.getContentPane().add(label_3, gbc_label_3);

		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_12 = new GridBagConstraints();
		gbc_horizontalStrut_12.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_12.gridx = 3;
		gbc_horizontalStrut_12.gridy = 9;
		frameSearch.getContentPane().add(horizontalStrut_12, gbc_horizontalStrut_12);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 3;
		gbc_horizontalStrut_9.gridy = 10;
		frameSearch.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 3;
		gbc_horizontalStrut_10.gridy = 11;
		frameSearch.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		// _____________________Name_______________________

		JCheckBox checkboxName = new JCheckBox("");
		checkboxName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_checkboxName = new GridBagConstraints();
		gbc_checkboxName.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxName.gridx = 1;
		gbc_checkboxName.gridy = 12;
		frameSearch.getContentPane().add(checkboxName, gbc_checkboxName);

		JLabel lblName = new JLabel("Name     :");
		lblName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.WEST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 2;
		gbc_lblName.gridy = 12;
		frameSearch.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		fieldName.setEnabled(false);
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 3;
		gbc_fieldName.gridy = 12;
		frameSearch.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 2;
		gbc_horizontalStrut_2.gridy = 13;
		frameSearch.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 2;
		gbc_horizontalStrut.gridy = 14;
		frameSearch.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		// _____________________Author_____________________

		JCheckBox checkboxAuthor = new JCheckBox("");
		checkboxAuthor.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_checkboxAuthor = new GridBagConstraints();
		gbc_checkboxAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxAuthor.gridx = 1;
		gbc_checkboxAuthor.gridy = 15;
		frameSearch.getContentPane().add(checkboxAuthor, gbc_checkboxAuthor);

		JLabel lblAuthor = new JLabel("Author    :");
		lblAuthor.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblAuthor = new GridBagConstraints();
		gbc_lblAuthor.anchor = GridBagConstraints.WEST;
		gbc_lblAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_lblAuthor.gridx = 2;
		gbc_lblAuthor.gridy = 15;
		frameSearch.getContentPane().add(lblAuthor, gbc_lblAuthor);

		fieldAuthor = new JTextField();
		fieldAuthor.setEnabled(false);
		GridBagConstraints gbc_fieldAuthor = new GridBagConstraints();
		gbc_fieldAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_fieldAuthor.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldAuthor.gridx = 3;
		gbc_fieldAuthor.gridy = 15;
		frameSearch.getContentPane().add(fieldAuthor, gbc_fieldAuthor);
		fieldAuthor.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 3;
		gbc_horizontalStrut_6.gridy = 16;
		frameSearch.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 3;
		gbc_horizontalStrut_4.gridy = 17;
		frameSearch.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		// _____________________PubYear______________________

		JCheckBox checkboxPubYear = new JCheckBox("");
		checkboxPubYear.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxPubYear = new GridBagConstraints();
		gbc_checkboxPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxPubYear.gridx = 1;
		gbc_checkboxPubYear.gridy = 18;
		frameSearch.getContentPane().add(checkboxPubYear, gbc_checkboxPubYear);

		JLabel lblPubYear = new JLabel("PubYear  :");
		lblPubYear.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblPubYear = new GridBagConstraints();
		gbc_lblPubYear.anchor = GridBagConstraints.WEST;
		gbc_lblPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_lblPubYear.gridx = 2;
		gbc_lblPubYear.gridy = 18;
		frameSearch.getContentPane().add(lblPubYear, gbc_lblPubYear);

		// -----------Panel for PubYear spinner------------

		JPanel panelPubYear = new JPanel();
		GridBagConstraints gbc_panelPubYear = new GridBagConstraints();
		gbc_panelPubYear.anchor = GridBagConstraints.WEST;
		gbc_panelPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_panelPubYear.gridx = 3;
		gbc_panelPubYear.gridy = 18;
		frameSearch.getContentPane().add(panelPubYear, gbc_panelPubYear);

		JLabel label = new JLabel("13");
		panelPubYear.add(label);

		JSpinner spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		spinnerYear.setEnabled(false);
		panelPubYear.add(spinnerYear);

		// ________________________________________________

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 3;
		gbc_horizontalStrut_7.gridy = 19;
		frameSearch.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		// _____________________Gender_____________________

		JCheckBox checkboxCategory = new JCheckBox("");
		checkboxCategory.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxCategory = new GridBagConstraints();
		gbc_checkboxCategory.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxCategory.gridx = 1;
		gbc_checkboxCategory.gridy = 20;
		frameSearch.getContentPane().add(checkboxCategory, gbc_checkboxCategory);

		JLabel lblCategory = new JLabel("Category :");
		lblCategory.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblCategory = new GridBagConstraints();
		gbc_lblCategory.anchor = GridBagConstraints.WEST;
		gbc_lblCategory.insets = new Insets(0, 0, 5, 5);
		gbc_lblCategory.gridx = 2;
		gbc_lblCategory.gridy = 20;
		frameSearch.getContentPane().add(lblCategory, gbc_lblCategory);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 3;
		gbc_scrollPane.gridy = 20;
		frameSearch.getContentPane().add(scrollPane, gbc_scrollPane);

		comboboxCategory = new JComboBox();
		comboboxCategory.setEnabled(false);
		comboboxCategory.setMaximumRowCount(12);
		comboboxCategory.setModel(new DefaultComboBoxModel(
				new String[] { "", "Sciences", "Computer", "Religion", "Refrence", "Date", "Story" }));
		scrollPane.setViewportView(comboboxCategory);

		// ________________________________________________

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 3;
		gbc_horizontalStrut_8.gridy = 21;
		frameSearch.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 3;
		gbc_horizontalStrut_3.gridy = 22;
		frameSearch.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 3;
		gbc_horizontalStrut_1.gridy = 23;
		frameSearch.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Search Button_________________

		JButton btnSearch = new JButton("Search!");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BooksList.resetTable();
				// get data from fields
				String id = fieldId.getText();
				String name = fieldName.getText();
				String author = fieldAuthor.getText();
				byte year = (byte) spinnerYear.getValue();
				// validate
				if (!checkboxId.isSelected() && !checkboxName.isSelected() && !checkboxAuthor.isSelected()
						&& !checkboxPubYear.isSelected() && !checkboxCategory.isSelected()) {
					Library.alert("Nothig selected!", "Search Book Error!", true, true);
					return;
				}
				if (checkboxId.isSelected()) {
					if (id.equals("")) {
						Library.alert("PLease enter \"Book ID\"!", "Search Book Error!", true, true);
						return;
					}
					try {
						Integer.parseInt(id);
					} catch (NumberFormatException e) {
						Library.alert("Invalid \"ID number\"!", "Search Book Error!", true, true);
						return;
					}
				}
				if (checkboxName.isSelected()) {
					if (name.equals("")) {
						Library.alert("PLease enter \"Name\"!", "Search Book Error!", true, true);
						return;
					}
				}
				if (checkboxAuthor.isSelected()) {
					if (author.equals("")) {
						Library.alert("PLease enter \"Author\"!", "Search Book Error!", true, true);
						return;
					}
				}
				if (checkboxPubYear.isSelected()) {
					if (year == 0) {
						Library.alert("PLease select \"Publish Year\"!", "Search Book Error!", true, true);
						return;
					}
				}
				// search
				int foundCount = 0;
				if (checkboxId.isSelected()) { // search by id = 0/1 result
					int idInt = Integer.parseInt(id);
					if (Library.book.searchById(idInt) != null) {
						BooksList.addTableRow((Book) Library.book.searchById(idInt));
						foundCount++;
					}
				} else { // search by other properties = 0/1/many result
					for (int i = 0; Library.book.getObject(i) != null; i++) {
						Book b = (Book) Library.book.getObject(i);
						// name❓
						if (checkboxName.isSelected()) {
							if (!b.getName().toLowerCase().contains(name.toLowerCase()))
								continue;// name❎
						}
						// name✔ author❓
						if (checkboxAuthor.isSelected()) {
							if (!b.getAuthor().toLowerCase().contains(author.toLowerCase()))
								continue;// author❎
						}
						// name✔ author✔ year❓
						if (checkboxPubYear.isSelected()) {
							if (year != 0)
								if (b.getPubYear() != year)
									continue;// date❎
						}
						// name✔ author✔ year✔
						
						BooksList.addTableRow(b);
						foundCount++;
					}
				}
				if (foundCount == 0)
					Library.alert("Book not found!", "Book Search Error!", false, true);
				else
					BooksList.main(null);
			}
		});
		btnSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearch.gridwidth = 3;
		gbc_btnSearch.gridx = 1;
		gbc_btnSearch.gridy = 24;
		frameSearch.getContentPane().add(btnSearch, gbc_btnSearch);
		;

		// __________________Label Click___________________

		lblId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxId.isSelected()) {
					checkboxId.setSelected(true);
				} else if (checkboxId.isSelected()) {
					checkboxId.setSelected(false);
				}
			}
		});
		lblName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxName.isSelected()) {
					checkboxName.setSelected(true);
				} else if (checkboxName.isSelected()) {
					checkboxName.setSelected(false);
				}
			}
		});
		lblAuthor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxAuthor.isSelected()) {
					checkboxAuthor.setSelected(true);
				} else if (checkboxAuthor.isSelected()) {
					checkboxAuthor.setSelected(false);
				}
			}
		});
		lblPubYear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxPubYear.isSelected()) {
					checkboxPubYear.setSelected(true);
				} else if (checkboxPubYear.isSelected()) {
					checkboxPubYear.setSelected(false);
				}
			}
		});
		lblCategory.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxCategory.isSelected()) {
					checkboxCategory.setSelected(true);
				} else if (checkboxCategory.isSelected()) {
					checkboxCategory.setSelected(false);
				}
			}
		});

		// ________________Check Box Click__________________

		checkboxId.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldId.setEnabled(true);
					lblId.setEnabled(true);
					// disable down
					lblName.setEnabled(false);
					lblAuthor.setEnabled(false);
					lblPubYear.setEnabled(false);
					lblCategory.setEnabled(false);
					checkboxName.setSelected(false);
					checkboxAuthor.setSelected(false);
					checkboxPubYear.setSelected(false);
					checkboxCategory.setSelected(false);
				}
				if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldId.setEnabled(false);
					lblName.setEnabled(true);
					lblAuthor.setEnabled(true);
					lblPubYear.setEnabled(true);
					lblCategory.setEnabled(true);
				}

			}
		});
		checkboxName.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldName.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblAuthor.setEnabled(true);
					lblPubYear.setEnabled(true);
					lblCategory.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldName.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxAuthor.isSelected() && !checkboxPubYear.isSelected()
							&& !checkboxCategory.isSelected())
						lblId.setEnabled(true);
				}
			}
		});
		checkboxAuthor.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldAuthor.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblAuthor.setEnabled(true);
					lblPubYear.setEnabled(true);
					lblCategory.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldAuthor.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxAuthor.isSelected() && !checkboxPubYear.isSelected()
							&& !checkboxCategory.isSelected())
						lblId.setEnabled(true);
				}
			}
		});
		checkboxPubYear.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					spinnerYear.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblAuthor.setEnabled(true);
					lblPubYear.setEnabled(true);
					lblCategory.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					spinnerYear.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxAuthor.isSelected() && !checkboxPubYear.isSelected()
							&& !checkboxCategory.isSelected())
						lblId.setEnabled(true);
				}

			}
		});
		checkboxCategory.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					comboboxCategory.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblAuthor.setEnabled(true);
					lblPubYear.setEnabled(true);
					lblCategory.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					comboboxCategory.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxAuthor.isSelected() && !checkboxPubYear.isSelected()
							&& !checkboxCategory.isSelected())
						lblId.setEnabled(true);
				}

			}
		});
	}
}
