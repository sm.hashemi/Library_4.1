package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.Library;
import model.Logger;
import model.Book;

import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionEvent;

public class BookUpdate {

	private static JFrame frameUpdateBook;
	private static JTextField fieldAuthor;
	private static JTextField fieldName;
	private static JTextField fieldId;
	private static JButton btnUpdate;
	private static JSpinner spinnerYear;
	private static JComboBox<?> comboboxCategory;
	private static Book found;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameUpdateBook();
		frameUpdateBook.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void initializeFrameUpdateBook() {
		frameUpdateBook = new JFrame();
		frameUpdateBook.setTitle("Update Book");
		frameUpdateBook.setVisible(true);
		frameUpdateBook.setBounds(100, 100, 380, 400);
		frameUpdateBook.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 16, 36, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 20, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frameUpdateBook.getContentPane().setLayout(gridBagLayout);

		JLabel lblId = new JLabel("ID :");
		lblId.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.anchor = GridBagConstraints.EAST;
		gbc_lblId.insets = new Insets(0, 0, 5, 5);
		gbc_lblId.gridx = 1;
		gbc_lblId.gridy = 1;
		frameUpdateBook.getContentPane().add(lblId, gbc_lblId);

		fieldId = new JTextField();
		;
		GridBagConstraints gbc_fieldId = new GridBagConstraints();
		gbc_fieldId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldId.gridx = 2;
		gbc_fieldId.gridy = 1;
		frameUpdateBook.getContentPane().add(fieldId, gbc_fieldId);
		fieldId.setColumns(10);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 1;
		gbc_horizontalStrut_11.gridy = 2;
		frameUpdateBook.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 1;
		gbc_horizontalStrut_5.gridy = 3;
		frameUpdateBook.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		JButton btnFind = new JButton("Find!");

		GridBagConstraints gbc_btnFind = new GridBagConstraints();
		gbc_btnFind.gridwidth = 2;
		gbc_btnFind.insets = new Insets(0, 0, 5, 5);
		gbc_btnFind.gridx = 1;
		gbc_btnFind.gridy = 4;
		frameUpdateBook.getContentPane().add(btnFind, gbc_btnFind);

		// ________________________________________________

		JLabel label_3 = new JLabel("__________________________________________");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridwidth = 2;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 5;
		frameUpdateBook.getContentPane().add(label_3, gbc_label_3);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 2;
		gbc_horizontalStrut_10.gridy = 6;
		frameUpdateBook.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 2;
		gbc_horizontalStrut_9.gridy = 7;
		frameUpdateBook.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		JLabel lblName = new JLabel("Name :");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 8;
		frameUpdateBook.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		fieldName.setEnabled(false);
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 2;
		gbc_fieldName.gridy = 8;
		frameUpdateBook.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 9;
		frameUpdateBook.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 10;
		frameUpdateBook.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblAuthor = new JLabel("Author :");

		GridBagConstraints gbc_lblAuthor = new GridBagConstraints();
		gbc_lblAuthor.anchor = GridBagConstraints.EAST;
		gbc_lblAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_lblAuthor.gridx = 1;
		gbc_lblAuthor.gridy = 11;
		frameUpdateBook.getContentPane().add(lblAuthor, gbc_lblAuthor);

		fieldAuthor = new JTextField();
		fieldAuthor.setEnabled(false);
		GridBagConstraints gbc_fieldAuthor = new GridBagConstraints();
		gbc_fieldAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_fieldAuthor.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldAuthor.gridx = 2;
		gbc_fieldAuthor.gridy = 11;
		frameUpdateBook.getContentPane().add(fieldAuthor, gbc_fieldAuthor);
		fieldAuthor.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 12;
		frameUpdateBook.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 13;
		frameUpdateBook.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		JLabel lblPubYear = new JLabel("PubYear :");

		GridBagConstraints gbc_lblPubYear = new GridBagConstraints();
		gbc_lblPubYear.anchor = GridBagConstraints.EAST;
		gbc_lblPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_lblPubYear.gridx = 1;
		gbc_lblPubYear.gridy = 14;
		frameUpdateBook.getContentPane().add(lblPubYear, gbc_lblPubYear);

		// -----------Panel for PubYear spinner------------

		JPanel panelPubYear = new JPanel();
		GridBagConstraints gbc_panelPubYear = new GridBagConstraints();
		gbc_panelPubYear.anchor = GridBagConstraints.WEST;
		gbc_panelPubYear.insets = new Insets(0, 0, 5, 5);
		gbc_panelPubYear.gridx = 2;
		gbc_panelPubYear.gridy = 14;
		frameUpdateBook.getContentPane().add(panelPubYear, gbc_panelPubYear);

		JLabel label = new JLabel("13");
		panelPubYear.add(label);

		spinnerYear = new JSpinner();
		spinnerYear.setEnabled(false);
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		panelPubYear.add(spinnerYear);

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 2;
		gbc_horizontalStrut_7.gridy = 15;
		frameUpdateBook.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JLabel lblCategory = new JLabel("Category :");

		GridBagConstraints gbc_lblCategory = new GridBagConstraints();
		gbc_lblCategory.anchor = GridBagConstraints.EAST;
		gbc_lblCategory.insets = new Insets(0, 0, 5, 5);
		gbc_lblCategory.gridx = 1;
		gbc_lblCategory.gridy = 16;
		frameUpdateBook.getContentPane().add(lblCategory, gbc_lblCategory);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 16;
		frameUpdateBook.getContentPane().add(scrollPane, gbc_scrollPane);

		comboboxCategory = new JComboBox();
		comboboxCategory.setEnabled(false);
		comboboxCategory.setName("");
		comboboxCategory.setMaximumRowCount(12);
		comboboxCategory.setModel(new DefaultComboBoxModel(
				new String[] { "", "Sciences", "Computer", "Religion", "Refrence", "Date", "Story" }));
		comboboxCategory.setSelectedIndex(0);
		scrollPane.setViewportView(comboboxCategory);

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 17;
		frameUpdateBook.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 18;
		frameUpdateBook.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 19;
		frameUpdateBook.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Update Button_________________

		btnUpdate = new JButton("Update!");
		btnUpdate.setEnabled(false);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get data from fields
				String name = fieldName.getText();
				String author = fieldAuthor.getText();
				byte year = (byte) spinnerYear.getValue();
				String category = (String) comboboxCategory.getSelectedItem();
				// validate
				if (name.equals("")) {
					Library.alert("Please enter \"Name\"!", "Update Book Error!", true, true);
					return;
				}
				if (author.equals("")) {
					Library.alert("Please enter \"Author\"!", "Update Book Error!", true, true);
					return;
				}
				if (year == 0) {
					Library.alert("Please select \"Publish Year\"!", "Update Book Error!", true, true);
					return;
				}
				// update data
				found.setName(name);
				found.setAuthor(author);
				found.setPubYear(year);
				found.setCategory(category);
				Logger.logBokUpd(found);
				Library.alert("✔ Data Updated Successfully!", "Done!", false, true);
				resetForm();
				return;
			}
		});

		btnUpdate.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdate.gridwidth = 2;
		gbc_btnUpdate.gridx = 1;
		gbc_btnUpdate.gridy = 20;
		frameUpdateBook.getContentPane().add(btnUpdate, gbc_btnUpdate);

		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldId.getText();
				int idInt;
				// validate
				if (id.equals("")) {
					Library.alert("Please enter \"Book ID\"!", "Find Book Error!", true, true);
					resetForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 100 || idInt > Book.getIdIndex()) {
						Library.alert("Invalid \"ID range\"!", "Find Book Error!", true, true);
						resetForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"ID number\"!", "Find Book Error!", true, true);
					resetForm();
					return;
				}
				// search and update
				if (Library.book.searchById(idInt) != null) {
					found = (Book) Library.book.searchById(idInt);
					btnUpdate.setEnabled(true);
					fieldName.setText(found.getName());
					fieldName.setEnabled(true);
					fieldAuthor.setText(found.getAuthor());
					fieldAuthor.setEnabled(true);
					spinnerYear.setValue(found.getPubYear());
					spinnerYear.setEnabled(true);
					comboboxCategory.setEnabled(true);
					comboboxCategory.setSelectedItem(found.getCategory());
					return;
				} else {
					Library.alert("Book not found!", "Find Book Error!", true, true);
					resetForm();
				}
			}

		});

	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetForm() {
		btnUpdate.setEnabled(false);
		fieldName.setEnabled(false);
		fieldName.setText("");
		fieldAuthor.setEnabled(false);
		fieldAuthor.setText("");
		spinnerYear.setEnabled(false);
		spinnerYear.setValue((byte) 0);
		comboboxCategory.setEnabled(false);
		comboboxCategory.setSelectedItem("");
	}
}
