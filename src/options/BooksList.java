package options;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import java.awt.GridBagConstraints;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Book;

public class BooksList {

	private static JFrame frameBooksList;
	private static JTable table;
	@SuppressWarnings("serial")
	private static DefaultTableModel model = new DefaultTableModel(0, 7) {
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameFoundedBook();
		frameBooksList.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameFoundedBook() {
		frameBooksList = new JFrame();
		frameBooksList.setTitle("Books List");
		frameBooksList.setBounds(100, 100, 900, 500);
		frameBooksList.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frameBooksList.getContentPane().setLayout(gridBagLayout);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		frameBooksList.getContentPane().add(scrollPane, gbc_scrollPane);

		table = new JTable();
		String[] cols = { "Book ID", "Name", "Author", "Category", "Publish Year", "Is Borrowed?", "Borrower ID" };
		model.setColumnIdentifiers(cols);
		table.setModel(model);
		scrollPane.setViewportView(table);
	}

	/**
	 * Add row to table with object data
	 */
	public static void addTableRow(Book b) {
		String isBorrow = null;
		if (b.isBorrowed()) {
			isBorrow = "✔";
			model.addRow(new String[] { String.valueOf(b.getId()), b.getName(), b.getAuthor(), b.getCategory(), "13" + b.getPubYearString(), isBorrow, String.valueOf(b.getBorrowerId()) });
		} else {
			isBorrow = "✖";
			model.addRow(new String[] { String.valueOf(b.getId()), b.getName(), b.getAuthor(), b.getCategory(), "13" + b.getPubYearString(), isBorrow });
		}
	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetTable() {
		model.setRowCount(0);
	}
}
