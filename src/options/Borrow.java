package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Cursor;

import model.Book;
import model.Library;
import model.Logger;
import model.Member;

import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.SystemColor;

public class Borrow {

	private static JFrame frameBorrow;
	private static JTextField fieldMemberFamily;
	private static JTextField fieldMemberName;
	private static JTextField fieldMemberId;
	private static JTextField fieldBookId;
	private static JTextField fieldBookName;
	private static JTextField fieldBookAuthor;
	private static JProgressBar progressBar;
	private static JCheckBox checkboxMember;
	private static JCheckBox checkboxBook;
	private static JButton btnBorrow;
	private static int progressByMember = 1;
	private static int progressByBook = 1;
	private static Member foundM;
	private static Book foundB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameBorrow();
		frameBorrow.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameBorrow() {
		frameBorrow = new JFrame();
		frameBorrow.setTitle("Borrow Book");
		frameBorrow.setVisible(true);
		frameBorrow.setBounds(100, 100, 472, 472);
		frameBorrow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 36, 80, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, -16, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		frameBorrow.getContentPane().setLayout(gridBagLayout);

		JLabel lblMemberId = new JLabel("Member ID :");
		lblMemberId.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblMemberId = new GridBagConstraints();
		gbc_lblMemberId.anchor = GridBagConstraints.EAST;
		gbc_lblMemberId.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberId.gridx = 0;
		gbc_lblMemberId.gridy = 0;
		frameBorrow.getContentPane().add(lblMemberId, gbc_lblMemberId);

		fieldMemberId = new JTextField();
		;
		GridBagConstraints gbc_fieldMemberId = new GridBagConstraints();
		gbc_fieldMemberId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberId.gridx = 1;
		gbc_fieldMemberId.gridy = 0;
		frameBorrow.getContentPane().add(fieldMemberId, gbc_fieldMemberId);
		fieldMemberId.setColumns(10);

		JButton btnFindMember = new JButton("Find Member!");
		btnFindMember.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_btnFindMember = new GridBagConstraints();
		gbc_btnFindMember.anchor = GridBagConstraints.WEST;
		gbc_btnFindMember.insets = new Insets(0, 0, 5, 0);
		gbc_btnFindMember.gridx = 2;
		gbc_btnFindMember.gridy = 0;
		frameBorrow.getContentPane().add(btnFindMember, gbc_btnFindMember);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 0;
		gbc_horizontalStrut_11.gridy = 1;
		frameBorrow.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 1;
		gbc_horizontalStrut_9.gridy = 2;
		frameBorrow.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		JLabel lblMemberName = new JLabel("Name :");
		lblMemberName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		GridBagConstraints gbc_lblMemberName = new GridBagConstraints();
		gbc_lblMemberName.anchor = GridBagConstraints.EAST;
		gbc_lblMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberName.gridx = 0;
		gbc_lblMemberName.gridy = 3;
		frameBorrow.getContentPane().add(lblMemberName, gbc_lblMemberName);

		fieldMemberName = new JTextField();
		fieldMemberName.setEditable(false);
		GridBagConstraints gbc_fieldMemberName = new GridBagConstraints();
		gbc_fieldMemberName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberName.gridx = 1;
		gbc_fieldMemberName.gridy = 3;
		frameBorrow.getContentPane().add(fieldMemberName, gbc_fieldMemberName);
		fieldMemberName.setColumns(10);

		JPanel panelCheckMember = new JPanel();
		GridBagConstraints gbc_panelCheckMember = new GridBagConstraints();
		gbc_panelCheckMember.anchor = GridBagConstraints.WEST;
		gbc_panelCheckMember.gridheight = 2;
		gbc_panelCheckMember.insets = new Insets(0, 0, 5, 0);
		gbc_panelCheckMember.gridx = 2;
		gbc_panelCheckMember.gridy = 3;
		frameBorrow.getContentPane().add(panelCheckMember, gbc_panelCheckMember);

		checkboxMember = new JCheckBox("");
		checkboxMember.setBackground(SystemColor.menu);
		checkboxMember.setEnabled(false);
		panelCheckMember.add(checkboxMember);

		JLabel lblCanBorrow = new JLabel("Can Borrow?");
		panelCheckMember.add(lblCanBorrow);

		JLabel lblFamily = new JLabel("Family :");
		lblFamily.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblFamily = new GridBagConstraints();
		gbc_lblFamily.anchor = GridBagConstraints.EAST;
		gbc_lblFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblFamily.gridx = 0;
		gbc_lblFamily.gridy = 4;
		frameBorrow.getContentPane().add(lblFamily, gbc_lblFamily);

		fieldMemberFamily = new JTextField();
		fieldMemberFamily.setEditable(false);
		GridBagConstraints gbc_fieldMemberFamily = new GridBagConstraints();
		gbc_fieldMemberFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberFamily.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberFamily.gridx = 1;
		gbc_fieldMemberFamily.gridy = 4;
		frameBorrow.getContentPane().add(fieldMemberFamily, gbc_fieldMemberFamily);
		fieldMemberFamily.setColumns(10);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 5;
		frameBorrow.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel label = new JLabel("__________________________________________");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridwidth = 3;
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 0;
		gbc_label.gridy = 6;
		frameBorrow.getContentPane().add(label, gbc_label);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 1;
		gbc_horizontalStrut_6.gridy = 7;
		frameBorrow.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 1;
		gbc_horizontalStrut_4.gridy = 8;
		frameBorrow.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 1;
		gbc_horizontalStrut_1.gridy = 9;
		frameBorrow.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		JLabel lblBookId = new JLabel("Book ID :");
		GridBagConstraints gbc_lblBookId = new GridBagConstraints();
		gbc_lblBookId.anchor = GridBagConstraints.EAST;
		gbc_lblBookId.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookId.gridx = 0;
		gbc_lblBookId.gridy = 10;
		frameBorrow.getContentPane().add(lblBookId, gbc_lblBookId);

		fieldBookId = new JTextField();
		fieldBookId.setColumns(10);
		GridBagConstraints gbc_fieldBookId = new GridBagConstraints();
		gbc_fieldBookId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookId.gridx = 1;
		gbc_fieldBookId.gridy = 10;
		frameBorrow.getContentPane().add(fieldBookId, gbc_fieldBookId);

		// __________________Update Button_________________

		btnBorrow = new JButton("Borrow!");
		btnBorrow.setEnabled(false);
		btnBorrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				foundB.setAsBorrowed();
				foundB.setBorrowerId(foundM.getId());
				foundB.setBorrowDateAsToday();
				foundM.setAsBorrower();
				foundM.setBorrowedBookId(foundB.getId());
				foundM.setBorrowDateAsToday();
				Logger.logBorrow(foundM.getId(), foundB.getId());
				Library.alert("✔ Registered Successfully!", "Borrow Done!", false, true);
				resetMemberForm();
				resetBookForm();
			}
		});

		JButton btnFindBook = new JButton("Find Book!");
		btnFindBook.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnFindBook.setMinimumSize(new Dimension(111, 25));
		btnFindBook.setPreferredSize(new Dimension(111, 25));
		GridBagConstraints gbc_btnFindBook = new GridBagConstraints();
		gbc_btnFindBook.anchor = GridBagConstraints.WEST;
		gbc_btnFindBook.insets = new Insets(0, 0, 5, 0);
		gbc_btnFindBook.gridx = 2;
		gbc_btnFindBook.gridy = 10;
		frameBorrow.getContentPane().add(btnFindBook, gbc_btnFindBook);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 1;
		gbc_horizontalStrut_10.gridy = 11;
		frameBorrow.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 1;
		gbc_horizontalStrut_5.gridy = 12;
		frameBorrow.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		JLabel lblBookName = new JLabel("Name :");
		GridBagConstraints gbc_lblBookName = new GridBagConstraints();
		gbc_lblBookName.anchor = GridBagConstraints.EAST;
		gbc_lblBookName.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookName.gridx = 0;
		gbc_lblBookName.gridy = 13;
		frameBorrow.getContentPane().add(lblBookName, gbc_lblBookName);

		fieldBookName = new JTextField();
		fieldBookName.setEditable(false);
		fieldBookName.setColumns(10);
		GridBagConstraints gbc_fieldBookName = new GridBagConstraints();
		gbc_fieldBookName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookName.gridx = 1;
		gbc_fieldBookName.gridy = 13;
		frameBorrow.getContentPane().add(fieldBookName, gbc_fieldBookName);

		JLabel lblAuthor = new JLabel("Author :");
		GridBagConstraints gbc_lblAuthor = new GridBagConstraints();
		gbc_lblAuthor.anchor = GridBagConstraints.EAST;
		gbc_lblAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_lblAuthor.gridx = 0;
		gbc_lblAuthor.gridy = 14;
		frameBorrow.getContentPane().add(lblAuthor, gbc_lblAuthor);

		fieldBookAuthor = new JTextField();
		fieldBookAuthor.setEditable(false);
		fieldBookAuthor.setColumns(10);
		GridBagConstraints gbc_fieldBookAuthor = new GridBagConstraints();
		gbc_fieldBookAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookAuthor.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookAuthor.gridx = 1;
		gbc_fieldBookAuthor.gridy = 14;
		frameBorrow.getContentPane().add(fieldBookAuthor, gbc_fieldBookAuthor);

		JPanel panelCheckBook = new JPanel();
		GridBagConstraints gbc_panelCheckBook = new GridBagConstraints();
		gbc_panelCheckBook.anchor = GridBagConstraints.WEST;
		gbc_panelCheckBook.gridheight = 2;
		gbc_panelCheckBook.insets = new Insets(0, 0, 5, 0);
		gbc_panelCheckBook.gridx = 2;
		gbc_panelCheckBook.gridy = 13;
		frameBorrow.getContentPane().add(panelCheckBook, gbc_panelCheckBook);

		checkboxBook = new JCheckBox("");
		checkboxBook.setEnabled(false);
		panelCheckBook.add(checkboxBook);

		JLabel lblCanBorrowed = new JLabel("Can Borrowed?");
		panelCheckBook.add(lblCanBorrowed);

		Component horizontalStrut_17 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_17 = new GridBagConstraints();
		gbc_horizontalStrut_17.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_17.gridx = 0;
		gbc_horizontalStrut_17.gridy = 15;
		frameBorrow.getContentPane().add(horizontalStrut_17, gbc_horizontalStrut_17);

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 1;
		gbc_horizontalStrut_8.gridy = 16;
		frameBorrow.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_15 = new GridBagConstraints();
		gbc_horizontalStrut_15.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_15.gridx = 0;
		gbc_horizontalStrut_15.gridy = 17;
		frameBorrow.getContentPane().add(horizontalStrut_15, gbc_horizontalStrut_15);

		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_14 = new GridBagConstraints();
		gbc_horizontalStrut_14.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_14.gridx = 0;
		gbc_horizontalStrut_14.gridy = 18;
		frameBorrow.getContentPane().add(horizontalStrut_14, gbc_horizontalStrut_14);

		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_12 = new GridBagConstraints();
		gbc_horizontalStrut_12.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_12.gridx = 1;
		gbc_horizontalStrut_12.gridy = 19;
		frameBorrow.getContentPane().add(horizontalStrut_12, gbc_horizontalStrut_12);

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 1;
		gbc_horizontalStrut_7.gridy = 20;
		frameBorrow.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		Component horizontalStrut_16 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_16 = new GridBagConstraints();
		gbc_horizontalStrut_16.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_16.gridx = 0;
		gbc_horizontalStrut_16.gridy = 21;
		frameBorrow.getContentPane().add(horizontalStrut_16, gbc_horizontalStrut_16);

		Component horizontalStrut_13 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_13 = new GridBagConstraints();
		gbc_horizontalStrut_13.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_13.gridx = 1;
		gbc_horizontalStrut_13.gridy = 22;
		frameBorrow.getContentPane().add(horizontalStrut_13, gbc_horizontalStrut_13);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 0;
		gbc_horizontalStrut_3.gridy = 23;
		frameBorrow.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 0;
		gbc_horizontalStrut_2.gridy = 24;
		frameBorrow.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setMaximum(2);
		progressBar.setString("0 / 2");
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_progressBar.gridwidth = 3;
		gbc_progressBar.insets = new Insets(0, 0, 5, 0);
		gbc_progressBar.gridx = 0;
		gbc_progressBar.gridy = 25;
		frameBorrow.getContentPane().add(progressBar, gbc_progressBar);
		btnBorrow.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnBorrow = new GridBagConstraints();
		gbc_btnBorrow.gridwidth = 3;
		gbc_btnBorrow.gridx = 0;
		gbc_btnBorrow.gridy = 26;
		frameBorrow.getContentPane().add(btnBorrow, gbc_btnBorrow);

		btnFindMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldMemberId.getText();
				int idInt;
				// validate
				if (id.equals("")) {
					Library.alert("Please enter \"Member ID\"!", "Find Member Error!", true, true);
					resetMemberForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 1000 || idInt > Member.getIdIndex()) {
						Library.alert("Invalid \"Member ID range\"!", "Find Member Error!", true, true);
						resetMemberForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"Member ID number\"!", "Find Member Error!", true, true);
					resetMemberForm();
					return;
				}
				// search
				if (Library.member.searchById(idInt) != null) {
					foundM = (Member) Library.member.searchById(idInt);

					fieldMemberName.setText(foundM.getName());
					fieldMemberFamily.setText(foundM.getFamily());
					if (!foundM.isBorrower()) {
						progressMember();
						checkboxMember.setBackground(Color.GREEN);
						checkboxMember.setSelected(true);
					} else {
						checkboxMember.setBackground(Color.RED);
					}
				} else {
					Library.alert("Member not found!", "Find Member Error!", true, true);
					resetMemberForm();
				}
			}

		});

		btnFindBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldBookId.getText();
				int idInt;
				if (id.equals("")) {
					Library.alert("Please enter \"Book ID\"!", "Find Book Error!", true, true);
					resetBookForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 100 || idInt > Book.getIdIndex()) {
						Library.alert("Invalid \"Book ID range\"!", "Find Book Error!", true, true);
						resetBookForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"Book ID number\"!", "Find Book Error!", true, true);
					resetBookForm();
					return;
				}
				// search
				if (Library.book.searchById(idInt) != null) {
					foundB = (Book) Library.book.searchById(idInt);
					fieldBookName.setText(foundB.getName());
					fieldBookAuthor.setText(foundB.getAuthor());
					if (!foundB.isBorrowed()) {
						progressBook();
						checkboxBook.setBackground(Color.GREEN);
						checkboxBook.setSelected(true);
					} else {
						checkboxBook.setBackground(Color.RED);
					}
				} else {
					Library.alert("Book not found!", "Find Book Error!", true, true);
					resetBookForm();
				}
			}

		});
	}

	/**
	 * Reset the contents of borrow frame.
	 */
	public static void resetMemberForm() {
		if (progressByMember == 1)
			return;
		checkboxMember.setBackground(SystemColor.menu);
		checkboxMember.setSelected(false);
		fieldMemberName.setText("");
		fieldMemberFamily.setText("");
		progressByMember = -1;
		progressMember();
		progressByMember = 1;
	}

	/**
	 * Reset the contents of the book frame.
	 */
	public static void resetBookForm() {
		if (progressByBook == 1)
			return;
		checkboxBook.setBackground(SystemColor.menu);
		checkboxBook.setSelected(false);
		fieldBookName.setText("");
		fieldBookAuthor.setText("");
		progressByBook = -1;
		progressBook();
		progressByBook = 1;
	}

	/**
	 * Manage JProgressbar.
	 */
	public static void progressMember() {
		if (progressByMember == -1) {
			progressByMember = 0;
			if (progressBar.getValue() != 0) {
				progressBar.setValue(progressBar.getValue() - 1);
				if (progressBar.getValue() == 0) {
					progressBar.setString("0 / 2");
					btnBorrow.setEnabled(false);
				}
				if (progressBar.getValue() == 1) {
					progressBar.setString("1 / 2");
					btnBorrow.setEnabled(false);
				}
			}
		} else if (progressByMember == 0) {
			return;
		} else if (progressByMember == 1) {
			progressByMember = 0;
			progressBar.setValue(progressBar.getValue() + 1);
			if (progressBar.getValue() == 1) {
				progressBar.setString("1 / 2");
				btnBorrow.setEnabled(false);
			}
			if (progressBar.getValue() == 2) {
				progressBar.setString("2 / 2");
				btnBorrow.setEnabled(true);
			}
		}
	}

	/**
	 * Manage JProgressbar.
	 */
	public static void progressBook() {
		if (progressByBook == -1) {
			progressByBook = 0;
			if (progressBar.getValue() != 0) {
				progressBar.setValue(progressBar.getValue() - 1);
				if (progressBar.getValue() == 0) {
					progressBar.setString("0 / 2");
					btnBorrow.setEnabled(false);
				}
				if (progressBar.getValue() == 1) {
					progressBar.setString("1 / 2");
					btnBorrow.setEnabled(false);
				}
			}
		} else if (progressByBook == 0) {
			return;
		} else if (progressByBook == 1) {
			progressByBook = 0;
			progressBar.setValue(progressBar.getValue() + 1);
			if (progressBar.getValue() == 1) {
				progressBar.setString("1 / 2");
				btnBorrow.setEnabled(false);
			}
			if (progressBar.getValue() == 2) {
				progressBar.setString("2 / 2");
				btnBorrow.setEnabled(true);
			}
		}
	}
}
