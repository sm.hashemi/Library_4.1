package options;
import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;
import model.Library;
import model.Logger;
import model.Member;

import javax.swing.SpinnerListModel;
import javax.swing.ButtonGroup;
import java.awt.event.ActionEvent;

public class MemberAdd {

	private static JFrame frameAddMember;
	private static JTextField fieldName;
	private static JTextField fieldFamily;
	private static JSpinner spinnerYear;
	private static JSpinner spinnerMonth;
	private static JSpinner spinnerDay;
	private static JRadioButton rdbtnMale;
	private static JRadioButton rdbtnFemale;
	private static ButtonGroup buttonGroup;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		InitializeFrameAddMember();
		frameAddMember.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void InitializeFrameAddMember() {
		frameAddMember = new JFrame();
		frameAddMember.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frameAddMember.setTitle("Add Member");
		frameAddMember.setVisible(true);
		frameAddMember.setBounds(100, 100, 381, 373);
		frameAddMember.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 16, 36, 200 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 25, 0, 0, 20, -8, 25, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0 };
		frameAddMember.getContentPane().setLayout(gridBagLayout);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_10.gridx = 2;
		gbc_horizontalStrut_10.gridy = 0;
		frameAddMember.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		JLabel lblName = new JLabel("Name :");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		frameAddMember.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 0);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 2;
		gbc_fieldName.gridy = 1;
		frameAddMember.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 2;
		frameAddMember.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 3;
		frameAddMember.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblFamily = new JLabel("Family :");

		GridBagConstraints gbc_lblFamily = new GridBagConstraints();
		gbc_lblFamily.anchor = GridBagConstraints.EAST;
		gbc_lblFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblFamily.gridx = 1;
		gbc_lblFamily.gridy = 4;
		frameAddMember.getContentPane().add(lblFamily, gbc_lblFamily);

		fieldFamily = new JTextField();
		GridBagConstraints gbc_fieldFamily = new GridBagConstraints();
		gbc_fieldFamily.insets = new Insets(0, 0, 5, 0);
		gbc_fieldFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldFamily.gridx = 2;
		gbc_fieldFamily.gridy = 4;
		frameAddMember.getContentPane().add(fieldFamily, gbc_fieldFamily);
		fieldFamily.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 5;
		frameAddMember.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 6;
		frameAddMember.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		JLabel lblAge = new JLabel("Birth :");

		GridBagConstraints gbc_lblAge = new GridBagConstraints();
		gbc_lblAge.anchor = GridBagConstraints.EAST;
		gbc_lblAge.insets = new Insets(0, 0, 5, 5);
		gbc_lblAge.gridx = 1;
		gbc_lblAge.gridy = 7;
		frameAddMember.getContentPane().add(lblAge, gbc_lblAge);

		// -----------Panel for 3 Birth spinner------------

		JPanel panelBirth = new JPanel();
		GridBagConstraints gbc_panelBirth = new GridBagConstraints();
		gbc_panelBirth.insets = new Insets(0, 0, 5, 0);
		gbc_panelBirth.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelBirth.gridx = 2;
		gbc_panelBirth.gridy = 7;
		frameAddMember.getContentPane().add(panelBirth, gbc_panelBirth);

		JLabel label = new JLabel("13");
		panelBirth.add(label);

		spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		panelBirth.add(spinnerYear);

		JLabel label_1 = new JLabel("/");
		panelBirth.add(label_1);

		spinnerMonth = new JSpinner();
		spinnerMonth.setModel(new SpinnerListModel(new String[] { "", "Farvardin", "Ordibehesht", "Khordad", "Tir",
				"Mordad", "Shahrivar", "Mehr", "Aban", "Azar", "Dey", "Bahman", "Esfand" }));
		spinnerMonth.setPreferredSize(new Dimension(80, 20));
		panelBirth.add(spinnerMonth);

		JLabel label_2 = new JLabel("/");
		panelBirth.add(label_2);

		spinnerDay = new JSpinner();
		spinnerDay.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 30),
				new Byte((byte) 1)));
		spinnerDay.setPreferredSize(new Dimension(35, 20));
		panelBirth.add(spinnerDay);

		// ________________________________________________

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_7.gridx = 2;
		gbc_horizontalStrut_7.gridy = 8;
		frameAddMember.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JLabel lblGender = new JLabel("Gen :");

		GridBagConstraints gbc_lblGender = new GridBagConstraints();
		gbc_lblGender.anchor = GridBagConstraints.EAST;
		gbc_lblGender.insets = new Insets(0, 0, 5, 5);
		gbc_lblGender.gridx = 1;
		gbc_lblGender.gridy = 9;
		frameAddMember.getContentPane().add(lblGender, gbc_lblGender);

		// ----------Panel for 2 Gen Radio Button----------

		JPanel panelGender = new JPanel();
		GridBagConstraints gbc_panelGender = new GridBagConstraints();
		gbc_panelGender.anchor = GridBagConstraints.WEST;
		gbc_panelGender.insets = new Insets(0, 0, 5, 0);
		gbc_panelGender.gridx = 2;
		gbc_panelGender.gridy = 9;
		frameAddMember.getContentPane().add(panelGender, gbc_panelGender);

		rdbtnMale = new JRadioButton("Male");
		buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnMale);
		rdbtnMale.setSelected(false);
		rdbtnMale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelGender.add(rdbtnMale);

		rdbtnFemale = new JRadioButton("Female\r\n");
		buttonGroup.add(rdbtnFemale);
		rdbtnFemale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelGender.add(rdbtnFemale);

		// ________________________________________________

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 10;
		frameAddMember.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 11;
		frameAddMember.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 12;
		frameAddMember.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Search Button_________________

		JButton btnAdd = new JButton("Add Member!");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get data from fields
				String name = fieldName.getText();
				String family = fieldFamily.getText();
				byte year = (byte) spinnerYear.getValue();
				byte month = Library.convertMonthToNum((String) spinnerMonth.getValue());
				byte day = (byte) spinnerDay.getValue();
				boolean gender;
				// Validate
				if (name.equals("")) {
					Library.alert("Please enter \"Name\"!", "Add Member Error!", true, true);
					return;
				}
				if (family.equals("")) {
					Library.alert("Please enter \"Family!", "Add Member Error!", true, true);
					return;
				}
				if (year == 0) {
					Library.alert("Please select \"Birth Year\"!", "Add Member Error!", true, true);
					return;
				}
				if (month == 0) {
					Library.alert("Please select \"Birth Month\"!", "Add Member Error!", true, true);
					return;
				}
				if (day == 0) {
					Library.alert("Please select \"Birth Day\"!", "Add Member Error!", true, true);
					return;
				}
				if (!rdbtnMale.isSelected() && !rdbtnFemale.isSelected()) {
					Library.alert("Please select \"Gender\"!", "Add Member Error!", true, true);
					return;
				}
				if(rdbtnMale.isSelected())
					gender = true;
				else
					gender = false;
				// Add Member
				Member m=new Member(name,family,year,month,day,gender);
				Library.member.add(m);
				Logger.logMemAdd(m);
				resetForm();
				return;
			}
		});
		btnAdd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.gridwidth = 2;
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 13;
		frameAddMember.getContentPane().add(btnAdd, gbc_btnAdd);
		;
	}

	
	
	/**
	 * Reset the contents of the frame.
	 */
	public static void resetForm() {
		fieldName.setText("");
		fieldFamily.setText("");
		spinnerYear.setValue((byte)0);
		spinnerMonth.setValue("");
		spinnerDay.setValue((byte)0);
		rdbtnMale.setSelected(false);
		rdbtnFemale.setSelected(false);
	}
}
