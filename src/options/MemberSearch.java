package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.*;

import javax.swing.SpinnerListModel;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

public class MemberSearch {

	private static JFrame frameSearch;
	private static JTextField fieldFamily;
	private static JTextField fieldName;
	private static ButtonGroup buttonGroup = new ButtonGroup();
	private static JTextField fieldId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameSearchMember();
		frameSearch.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameSearchMember() {
		frameSearch = new JFrame();
		frameSearch.setTitle("Advanced Search");
		frameSearch.setVisible(true);
		frameSearch.setBounds(100, 100, 380, 420);
		frameSearch.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 16, 36, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 20, -8, 25, 0, 0,
				0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frameSearch.getContentPane().setLayout(gridBagLayout);

		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_14 = new GridBagConstraints();
		gbc_horizontalStrut_14.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_14.gridx = 3;
		gbc_horizontalStrut_14.gridy = 0;
		frameSearch.getContentPane().add(horizontalStrut_14, gbc_horizontalStrut_14);

		JLabel topLine = new JLabel("-------- Advane Search is Here! --------");
		GridBagConstraints gbc_topLine = new GridBagConstraints();
		gbc_topLine.gridwidth = 3;
		gbc_topLine.insets = new Insets(0, 0, 5, 5);
		gbc_topLine.gridx = 1;
		gbc_topLine.gridy = 1;
		frameSearch.getContentPane().add(topLine, gbc_topLine);

		JLabel topLine2 = new JLabel("* Use one or More option! ");
		GridBagConstraints gbc_topLine2 = new GridBagConstraints();
		gbc_topLine2.gridwidth = 3;
		gbc_topLine2.insets = new Insets(0, 0, 5, 5);
		gbc_topLine2.gridx = 1;
		gbc_topLine2.gridy = 2;
		frameSearch.getContentPane().add(topLine2, gbc_topLine2);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 3;
		gbc_horizontalStrut_5.gridy = 3;
		frameSearch.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		Component horizontalStrut_13 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_13 = new GridBagConstraints();
		gbc_horizontalStrut_13.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_13.gridx = 3;
		gbc_horizontalStrut_13.gridy = 4;
		frameSearch.getContentPane().add(horizontalStrut_13, gbc_horizontalStrut_13);

		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_15 = new GridBagConstraints();
		gbc_horizontalStrut_15.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_15.gridx = 3;
		gbc_horizontalStrut_15.gridy = 5;
		frameSearch.getContentPane().add(horizontalStrut_15, gbc_horizontalStrut_15);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 3;
		gbc_horizontalStrut_11.gridy = 6;
		frameSearch.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		JCheckBox checkboxId = new JCheckBox("");
		checkboxId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxId = new GridBagConstraints();
		gbc_checkboxId.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxId.gridx = 1;
		gbc_checkboxId.gridy = 7;
		frameSearch.getContentPane().add(checkboxId, gbc_checkboxId);

		JLabel lblId = new JLabel("ID       :");
		lblId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.anchor = GridBagConstraints.EAST;
		gbc_lblId.insets = new Insets(0, 0, 5, 5);
		gbc_lblId.gridx = 2;
		gbc_lblId.gridy = 7;
		frameSearch.getContentPane().add(lblId, gbc_lblId);

		fieldId = new JTextField();
		fieldId.setEnabled(false);
		;
		GridBagConstraints gbc_fieldId = new GridBagConstraints();
		gbc_fieldId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldId.gridx = 3;
		gbc_fieldId.gridy = 7;
		frameSearch.getContentPane().add(fieldId, gbc_fieldId);
		fieldId.setColumns(10);

		// _______________________+________________________
		// ________________________________________________

		JLabel label_3 = new JLabel("_____________________________________________");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridwidth = 3;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 8;
		frameSearch.getContentPane().add(label_3, gbc_label_3);

		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_12 = new GridBagConstraints();
		gbc_horizontalStrut_12.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_12.gridx = 3;
		gbc_horizontalStrut_12.gridy = 9;
		frameSearch.getContentPane().add(horizontalStrut_12, gbc_horizontalStrut_12);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 3;
		gbc_horizontalStrut_9.gridy = 10;
		frameSearch.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 3;
		gbc_horizontalStrut_10.gridy = 11;
		frameSearch.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		// _____________________Name_______________________

		JCheckBox checkboxName = new JCheckBox("");
		checkboxName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_checkboxName = new GridBagConstraints();
		gbc_checkboxName.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxName.gridx = 1;
		gbc_checkboxName.gridy = 12;
		frameSearch.getContentPane().add(checkboxName, gbc_checkboxName);

		JLabel lblName = new JLabel("Name  :");
		lblName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.WEST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 2;
		gbc_lblName.gridy = 12;
		frameSearch.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		fieldName.setEnabled(false);
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 3;
		gbc_fieldName.gridy = 12;
		frameSearch.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 2;
		gbc_horizontalStrut_2.gridy = 13;
		frameSearch.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 2;
		gbc_horizontalStrut.gridy = 14;
		frameSearch.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		// _____________________Family_____________________

		JCheckBox checkboxFamily = new JCheckBox("");
		checkboxFamily.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_checkboxFamily = new GridBagConstraints();
		gbc_checkboxFamily.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxFamily.gridx = 1;
		gbc_checkboxFamily.gridy = 15;
		frameSearch.getContentPane().add(checkboxFamily, gbc_checkboxFamily);

		JLabel lblFamily = new JLabel("Family :");
		lblFamily.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblFamily = new GridBagConstraints();
		gbc_lblFamily.anchor = GridBagConstraints.WEST;
		gbc_lblFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblFamily.gridx = 2;
		gbc_lblFamily.gridy = 15;
		frameSearch.getContentPane().add(lblFamily, gbc_lblFamily);

		fieldFamily = new JTextField();
		fieldFamily.setEnabled(false);
		GridBagConstraints gbc_fieldFamily = new GridBagConstraints();
		gbc_fieldFamily.insets = new Insets(0, 0, 5, 5);
		gbc_fieldFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldFamily.gridx = 3;
		gbc_fieldFamily.gridy = 15;
		frameSearch.getContentPane().add(fieldFamily, gbc_fieldFamily);
		fieldFamily.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 3;
		gbc_horizontalStrut_6.gridy = 16;
		frameSearch.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 3;
		gbc_horizontalStrut_4.gridy = 17;
		frameSearch.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		// _____________________Birth______________________

		JCheckBox checkboxAge = new JCheckBox("");
		checkboxAge.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxAge = new GridBagConstraints();
		gbc_checkboxAge.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxAge.gridx = 1;
		gbc_checkboxAge.gridy = 18;
		frameSearch.getContentPane().add(checkboxAge, gbc_checkboxAge);

		JLabel lblAge = new JLabel("Birth   :");
		lblAge.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblAge = new GridBagConstraints();
		gbc_lblAge.anchor = GridBagConstraints.WEST;
		gbc_lblAge.insets = new Insets(0, 0, 5, 5);
		gbc_lblAge.gridx = 2;
		gbc_lblAge.gridy = 18;
		frameSearch.getContentPane().add(lblAge, gbc_lblAge);

		// -----------Panel for 3 Birth spinner------------

		JPanel panelBirth = new JPanel();
		GridBagConstraints gbc_panelBirth = new GridBagConstraints();
		gbc_panelBirth.insets = new Insets(0, 0, 5, 5);
		gbc_panelBirth.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelBirth.gridx = 3;
		gbc_panelBirth.gridy = 18;
		frameSearch.getContentPane().add(panelBirth, gbc_panelBirth);

		JLabel label = new JLabel("13");
		panelBirth.add(label);

		JSpinner spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		spinnerYear.setEnabled(false);
		panelBirth.add(spinnerYear);

		JLabel label_1 = new JLabel("/");
		panelBirth.add(label_1);

		JSpinner spinnerMonth = new JSpinner();
		spinnerMonth.setModel(new SpinnerListModel(new String[] { "", "Farvardin", "Ordibehesht", "Khordad", "Tir",
				"Mordad", "Shahrivar", "Mehr", "Aban", "Azar", "Dey", "Bahman", "Esfand" }));
		spinnerMonth.setPreferredSize(new Dimension(80, 20));
		spinnerMonth.setEnabled(false);
		panelBirth.add(spinnerMonth);

		JLabel label_2 = new JLabel("/");
		panelBirth.add(label_2);

		JSpinner spinnerDay = new JSpinner();
		spinnerDay.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 30),
				new Byte((byte) 1)));
		spinnerDay.setPreferredSize(new Dimension(35, 20));
		spinnerDay.setEnabled(false);
		panelBirth.add(spinnerDay);

		// ________________________________________________

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 3;
		gbc_horizontalStrut_7.gridy = 19;
		frameSearch.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		// _____________________Gender_____________________

		JCheckBox checkboxGender = new JCheckBox("");
		checkboxGender.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_checkboxGender = new GridBagConstraints();
		gbc_checkboxGender.insets = new Insets(0, 0, 5, 5);
		gbc_checkboxGender.gridx = 1;
		gbc_checkboxGender.gridy = 20;
		frameSearch.getContentPane().add(checkboxGender, gbc_checkboxGender);

		JLabel lblGender = new JLabel("Gen    :");
		lblGender.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblGender = new GridBagConstraints();
		gbc_lblGender.anchor = GridBagConstraints.WEST;
		gbc_lblGender.insets = new Insets(0, 0, 5, 5);
		gbc_lblGender.gridx = 2;
		gbc_lblGender.gridy = 20;
		frameSearch.getContentPane().add(lblGender, gbc_lblGender);

		// ----------Panel for 2 Gender Radio Button----------

		JPanel panelGender = new JPanel();
		GridBagConstraints gbc_panelGender = new GridBagConstraints();
		gbc_panelGender.anchor = GridBagConstraints.WEST;
		gbc_panelGender.insets = new Insets(0, 0, 5, 5);
		gbc_panelGender.gridx = 3;
		gbc_panelGender.gridy = 20;
		frameSearch.getContentPane().add(panelGender, gbc_panelGender);

		JRadioButton rdbtnMale = new JRadioButton("Male");
		buttonGroup.add(rdbtnMale);
		rdbtnMale.setSelected(false);
		rdbtnMale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnMale.setEnabled(false);
		panelGender.add(rdbtnMale);

		JRadioButton rdbtnFemale = new JRadioButton("Female\r\n");
		buttonGroup.add(rdbtnFemale);
		rdbtnFemale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnFemale.setEnabled(false);
		panelGender.add(rdbtnFemale);

		// ________________________________________________

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 3;
		gbc_horizontalStrut_8.gridy = 21;
		frameSearch.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 3;
		gbc_horizontalStrut_3.gridy = 22;
		frameSearch.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 3;
		gbc_horizontalStrut_1.gridy = 23;
		frameSearch.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Search Button_________________

		JButton btnSearch = new JButton("Search!");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MembersList.resetTable();
				// get data from fields
				String id = fieldId.getText();
				String name = fieldName.getText();
				String family = fieldFamily.getText();
				byte year = (byte) spinnerYear.getValue();
				byte month = Library.convertMonthToNum((String) spinnerMonth.getValue());
				byte day = (byte) spinnerDay.getValue();
				// validate
				if (!checkboxId.isSelected() && !checkboxName.isSelected() && !checkboxFamily.isSelected()
						&& !checkboxAge.isSelected() && !checkboxGender.isSelected()) {
					Library.alert("Nothig selected!", "Search Member Error!", true, true);
					return;
				}
				if (checkboxId.isSelected()) {
					if (id.equals("")) {
						Library.alert("Please enter \"Member ID\"!", "Search Member Error!", true, true);
						return;
					}
					try {
						Integer.parseInt(id);
					} catch (NumberFormatException e) {
						Library.alert("Invalid \"ID Number\"!", "Search Member Error!", true, true);
						return;
					}
				}
				if (checkboxName.isSelected()) {
					if (name.equals("")) {
						Library.alert("Please enter \"Name\"!", "Search Member Error!", true, true);
						return;
					}
				}
				if (checkboxFamily.isSelected()) {
					if (family.equals("")) {
						Library.alert("Please enter \"Family!", "Search Member Error!", true, true);
						return;
					}
				}
				if (checkboxAge.isSelected()) {
					if (year == 0 && month == 0 && day == 0) {
						Library.alert("Please select \"Birth Date\"!", "Search Member Error!", true, true);
						return;
					}
				}
				if (checkboxGender.isSelected()) {
					if (!rdbtnMale.isSelected() && !rdbtnFemale.isSelected()) {
						Library.alert("Please select \"Gender\"!", "Search Member Error!", true, true);
						return;
					}
				}
				// search
				int foundCount = 0;
				if (checkboxId.isSelected()) { // search by id = 0/1 result
					int idInt = Integer.parseInt(id);
					if (Library.member.searchById(idInt) != null) {
						MembersList.addTableRow((Member) Library.member.searchById(idInt));
						foundCount++;
					}
				} else { // search by other properties = 0/1/many result
					for (int i = 0; Library.member.getObject(i) != null; i++) {
						Member m = (Member) Library.member.getObject(i);
						// name❓
						if (checkboxName.isSelected()) {
							if (!m.getName().toLowerCase().contains(name.toLowerCase()))
								continue;// name❎
						}
						// name✔ family❓
						if (checkboxFamily.isSelected()) {
							if (!m.getFamily().toLowerCase().contains(family.toLowerCase()))
								continue;// family❎
						}
						// name✔ family✔ age❓
						if (checkboxAge.isSelected()) {
							if (year != 0)
								if (m.getBirth().getYear() != year)
									continue;// date❎
							if (month != 0)
								if (m.getBirth().getMonthValue() != month)
									continue;// date❎
							if (day != 0)
								if (m.getBirth().getDayOfMonth() != day)
									continue;// date❎
						}
						// name✔ family✔ age✔ gender❓
						if (checkboxGender.isSelected()) {
							if (rdbtnMale.isSelected())
								if (!m.getGender())
									continue;// gender❎
								else if (rdbtnFemale.isSelected())
									if (m.getGender())
										continue;// gender❎
						}
						// name✔ family✔ age✔ gender✔
						MembersList.addTableRow(m);
						foundCount++;
					}
				}
				if (foundCount == 0)
					Library.alert("Member not found!", "Member Search Error!", false, true);
				else
					MembersList.main(null);
			}
		});
		btnSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.insets = new Insets(0, 0, 5, 5);
		gbc_btnSearch.gridwidth = 2;
		gbc_btnSearch.gridx = 2;
		gbc_btnSearch.gridy = 24;
		frameSearch.getContentPane().add(btnSearch, gbc_btnSearch);
		;

		// __________________Label Click___________________

		lblId.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxId.isSelected()) {
					checkboxId.setSelected(true);
				} else if (checkboxId.isSelected()) {
					checkboxId.setSelected(false);
				}
			}
		});
		lblName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxName.isSelected()) {
					checkboxName.setSelected(true);
				} else if (checkboxName.isSelected()) {
					checkboxName.setSelected(false);
				}
			}
		});
		lblFamily.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxFamily.isSelected()) {
					checkboxFamily.setSelected(true);
				} else if (checkboxFamily.isSelected()) {
					checkboxFamily.setSelected(false);
				}
			}
		});
		lblAge.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxAge.isSelected()) {
					checkboxAge.setSelected(true);
				} else if (checkboxAge.isSelected()) {
					checkboxAge.setSelected(false);
				}
			}
		});
		lblGender.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!checkboxGender.isSelected()) {
					checkboxGender.setSelected(true);
				} else if (checkboxGender.isSelected()) {
					checkboxGender.setSelected(false);
				}
			}
		});

		// ________________Check Box Click__________________

		checkboxId.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldId.setEnabled(true);
					lblId.setEnabled(true);
					// disable down
					lblName.setEnabled(false);
					lblFamily.setEnabled(false);
					lblAge.setEnabled(false);
					lblGender.setEnabled(false);
					checkboxName.setSelected(false);
					checkboxFamily.setSelected(false);
					checkboxAge.setSelected(false);
					checkboxGender.setSelected(false);
				}
				if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldId.setEnabled(false);
					lblName.setEnabled(true);
					lblFamily.setEnabled(true);
					lblAge.setEnabled(true);
					lblGender.setEnabled(true);
				}

			}
		});
		checkboxName.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldName.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblFamily.setEnabled(true);
					lblAge.setEnabled(true);
					lblGender.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldName.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxFamily.isSelected() && !checkboxAge.isSelected()
							&& !checkboxGender.isSelected())
						lblId.setEnabled(true);
				}
			}
		});
		checkboxFamily.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					fieldFamily.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblFamily.setEnabled(true);
					lblAge.setEnabled(true);
					lblGender.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					fieldFamily.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxFamily.isSelected() && !checkboxAge.isSelected()
							&& !checkboxGender.isSelected())
						lblId.setEnabled(true);
				}
			}
		});
		checkboxAge.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					spinnerYear.setEnabled(true);
					spinnerMonth.setEnabled(true);
					spinnerDay.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblFamily.setEnabled(true);
					lblAge.setEnabled(true);
					lblGender.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					spinnerYear.setEnabled(false);
					spinnerMonth.setEnabled(false);
					spinnerDay.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxFamily.isSelected() && !checkboxAge.isSelected()
							&& !checkboxGender.isSelected())
						lblId.setEnabled(true);
				}

			}
		});
		checkboxGender.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					rdbtnMale.setEnabled(true);
					rdbtnFemale.setEnabled(true);
					// disable up
					checkboxId.setSelected(false);
					lblId.setEnabled(false);
					// enable down
					lblName.setEnabled(true);
					lblFamily.setEnabled(true);
					lblAge.setEnabled(true);
					lblGender.setEnabled(true);
				} else if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					rdbtnMale.setEnabled(false);
					rdbtnFemale.setEnabled(false);
					if (!checkboxName.isSelected() && !checkboxFamily.isSelected() && !checkboxAge.isSelected()
							&& !checkboxGender.isSelected())
						lblId.setEnabled(true);
				}

			}
		});
	}
}