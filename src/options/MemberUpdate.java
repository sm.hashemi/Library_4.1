package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.Library;
import model.Logger;
import model.Member;

import javax.swing.SpinnerListModel;
import javax.swing.ButtonGroup;
import java.awt.event.ActionEvent;

public class MemberUpdate {

	private static JFrame frameUpdateMember;
	private static JTextField fieldFamily;
	private static JTextField fieldName;
	private static JButton btnUpdate;
	private static JSpinner spinnerYear;
	private static JSpinner spinnerMonth;
	private static JSpinner spinnerDay;
	private static JRadioButton rdbtnMale;
	private static JRadioButton rdbtnFemale;
	private final static ButtonGroup buttonGroup = new ButtonGroup();
	private static JTextField fieldId;
	private static Member found;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameUpdateMember();
		frameUpdateMember.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameUpdateMember() {
		frameUpdateMember = new JFrame();
		frameUpdateMember.setTitle("Update Member");
		frameUpdateMember.setVisible(true);
		frameUpdateMember.setBounds(100, 100, 380, 400);
		frameUpdateMember.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 16, 36, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 20, -8, 25, 0, 0, 0, 0,
				0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frameUpdateMember.getContentPane().setLayout(gridBagLayout);

		JLabel lblId = new JLabel("ID :");
		lblId.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.anchor = GridBagConstraints.EAST;
		gbc_lblId.insets = new Insets(0, 0, 5, 5);
		gbc_lblId.gridx = 1;
		gbc_lblId.gridy = 1;
		frameUpdateMember.getContentPane().add(lblId, gbc_lblId);

		fieldId = new JTextField();
		;
		GridBagConstraints gbc_fieldId = new GridBagConstraints();
		gbc_fieldId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldId.gridx = 2;
		gbc_fieldId.gridy = 1;
		frameUpdateMember.getContentPane().add(fieldId, gbc_fieldId);
		fieldId.setColumns(10);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 1;
		gbc_horizontalStrut_11.gridy = 2;
		frameUpdateMember.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 1;
		gbc_horizontalStrut_5.gridy = 3;
		frameUpdateMember.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		JButton btnFind = new JButton("Find!");

		GridBagConstraints gbc_btnFind = new GridBagConstraints();
		gbc_btnFind.gridwidth = 2;
		gbc_btnFind.insets = new Insets(0, 0, 5, 5);
		gbc_btnFind.gridx = 1;
		gbc_btnFind.gridy = 4;
		frameUpdateMember.getContentPane().add(btnFind, gbc_btnFind);

		// _______________________+________________________
		// ________________________________________________

		JLabel label_3 = new JLabel("__________________________________________");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridwidth = 2;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 5;
		frameUpdateMember.getContentPane().add(label_3, gbc_label_3);

		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_12 = new GridBagConstraints();
		gbc_horizontalStrut_12.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_12.gridx = 2;
		gbc_horizontalStrut_12.gridy = 6;
		frameUpdateMember.getContentPane().add(horizontalStrut_12, gbc_horizontalStrut_12);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 2;
		gbc_horizontalStrut_9.gridy = 7;
		frameUpdateMember.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 2;
		gbc_horizontalStrut_10.gridy = 8;
		frameUpdateMember.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		JLabel lblName = new JLabel("Name :");
		lblName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 9;
		frameUpdateMember.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		fieldName.setEnabled(false);
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 2;
		gbc_fieldName.gridy = 9;
		frameUpdateMember.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 10;
		frameUpdateMember.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 11;
		frameUpdateMember.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblFamily = new JLabel("Family :");
		lblFamily.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblFamily = new GridBagConstraints();
		gbc_lblFamily.anchor = GridBagConstraints.EAST;
		gbc_lblFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblFamily.gridx = 1;
		gbc_lblFamily.gridy = 12;
		frameUpdateMember.getContentPane().add(lblFamily, gbc_lblFamily);

		fieldFamily = new JTextField();
		fieldFamily.setEnabled(false);
		GridBagConstraints gbc_fieldFamily = new GridBagConstraints();
		gbc_fieldFamily.insets = new Insets(0, 0, 5, 5);
		gbc_fieldFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldFamily.gridx = 2;
		gbc_fieldFamily.gridy = 12;
		frameUpdateMember.getContentPane().add(fieldFamily, gbc_fieldFamily);
		fieldFamily.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 13;
		frameUpdateMember.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 14;
		frameUpdateMember.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		JLabel lblBirth = new JLabel("Birth :");
		lblBirth.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblBirth = new GridBagConstraints();
		gbc_lblBirth.anchor = GridBagConstraints.EAST;
		gbc_lblBirth.insets = new Insets(0, 0, 5, 5);
		gbc_lblBirth.gridx = 1;
		gbc_lblBirth.gridy = 15;
		frameUpdateMember.getContentPane().add(lblBirth, gbc_lblBirth);

		// -----------Panel for 3 Birth spinner------------

		JPanel panelBirth = new JPanel();
		GridBagConstraints gbc_panelBirth = new GridBagConstraints();
		gbc_panelBirth.insets = new Insets(0, 0, 5, 5);
		gbc_panelBirth.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelBirth.gridx = 2;
		gbc_panelBirth.gridy = 15;
		frameUpdateMember.getContentPane().add(panelBirth, gbc_panelBirth);

		JLabel label = new JLabel("13");
		panelBirth.add(label);

		spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		spinnerYear.setEnabled(false);
		panelBirth.add(spinnerYear);

		JLabel label_1 = new JLabel("/");
		panelBirth.add(label_1);

		spinnerMonth = new JSpinner();
		spinnerMonth.setModel(new SpinnerListModel(new String[] { "", "Farvardin", "Ordibehesht", "Khordad", "Tir",
				"Mordad", "Shahrivar", "Mehr", "Aban", "Azar", "Dey", "Bahman", "Esfand" }));
		spinnerMonth.setPreferredSize(new Dimension(80, 20));
		spinnerMonth.setEnabled(false);
		panelBirth.add(spinnerMonth);

		JLabel label_2 = new JLabel("/");
		panelBirth.add(label_2);

		spinnerDay = new JSpinner();
		spinnerDay.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 30),
				new Byte((byte) 1)));
		spinnerDay.setPreferredSize(new Dimension(35, 20));
		spinnerDay.setEnabled(false);
		panelBirth.add(spinnerDay);

		// ________________________________________________

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 2;
		gbc_horizontalStrut_7.gridy = 16;
		frameUpdateMember.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JLabel lblGender = new JLabel("Gender :");
		lblGender.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblGender = new GridBagConstraints();
		gbc_lblGender.anchor = GridBagConstraints.EAST;
		gbc_lblGender.insets = new Insets(0, 0, 5, 5);
		gbc_lblGender.gridx = 1;
		gbc_lblGender.gridy = 17;
		frameUpdateMember.getContentPane().add(lblGender, gbc_lblGender);

		// ----------Panel for 2 Gen Radio Button----------

		JPanel panelGender = new JPanel();
		GridBagConstraints gbc_panelGender = new GridBagConstraints();
		gbc_panelGender.anchor = GridBagConstraints.WEST;
		gbc_panelGender.insets = new Insets(0, 0, 5, 5);
		gbc_panelGender.gridx = 2;
		gbc_panelGender.gridy = 17;
		frameUpdateMember.getContentPane().add(panelGender, gbc_panelGender);

		rdbtnMale = new JRadioButton("Male");
		buttonGroup.add(rdbtnMale);
		rdbtnMale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnMale.setEnabled(false);
		panelGender.add(rdbtnMale);

		rdbtnFemale = new JRadioButton("Female\r\n");
		buttonGroup.add(rdbtnFemale);
		rdbtnFemale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnFemale.setEnabled(false);
		panelGender.add(rdbtnFemale);

		// ________________________________________________

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 18;
		frameUpdateMember.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 19;
		frameUpdateMember.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 20;
		frameUpdateMember.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Update Button_________________

		btnUpdate = new JButton("Update!");
		btnUpdate.setEnabled(false);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get data
				String name = fieldName.getText();
				String family = fieldFamily.getText();
				byte year = (byte) spinnerYear.getValue();
				byte month = Library.convertMonthToNum((String) spinnerMonth.getValue());
				byte day = (byte) spinnerDay.getValue();
				boolean gender;
				// Validate data
				if (name.equals("")) {
					Library.alert("Please Enter Name!", "Update Member Error!", true, true);
					return;
				}
				if (family.equals("")) {
					Library.alert("Please Enter Family!", "Update Member Error!", true, true);
					return;
				}
				if (year == 0 && month == 0 && day == 0) {
					Library.alert("Please Select Birth Date!", "Update Member Error!", true, true);
					return;
				}
				if (!rdbtnMale.isSelected() && !rdbtnFemale.isSelected()) {
					Library.alert("Please Select Gender!", "Update Member Error!", true, true);
					return;
				}
				if (rdbtnMale.isSelected())
					gender = true;
				else
					gender = false;
				// Update data
				found.setName(name);
				found.setFamily(family);
				found.setBirth(year, month, day);
				found.setGender(gender);
				Logger.logMemUpd(found);
				Library.alert("✔ Data Updated Successfully!", "Done!", false, true);
				resetForm();
			}
		});
		btnUpdate.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdate.gridwidth = 2;
		gbc_btnUpdate.gridx = 1;
		gbc_btnUpdate.gridy = 21;
		frameUpdateMember.getContentPane().add(btnUpdate, gbc_btnUpdate);

		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldId.getText();
				int idInt;
				// validate
				if (id.equals("")) {
					Library.alert("Please enter \"Member ID\"!", "Find Member Error!", true, true);
					resetForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 1000 || idInt > Member.getIdIndex()) {
						Library.alert("Invalid \"ID Range\"!", "Find Member Error!", true, true);
						resetForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"ID Number\"!", "Find Member Error!", true, true);
					resetForm();
					return;
				}
				// search & show information
				if (Library.member.searchById(idInt) != null) {
					found = (Member) Library.member.searchById(idInt);
					btnUpdate.setEnabled(true);
					fieldName.setText(found.getName());
					fieldName.setEnabled(true);
					fieldFamily.setText(found.getFamily());
					fieldFamily.setEnabled(true);
					spinnerYear.setValue(found.getBirthYear());
					spinnerYear.setEnabled(true);
					spinnerMonth.setValue(Library.convertNumToMonth(found.getBirthMonth()));
					spinnerMonth.setEnabled(true);
					spinnerDay.setValue(found.getBirthDay());
					spinnerDay.setEnabled(true);
					rdbtnMale.setEnabled(true);
					rdbtnFemale.setEnabled(true);
					if (found.getGender())
						rdbtnMale.setSelected(true);
					else
						rdbtnFemale.setSelected(true);
					return;
				} else {
					Library.alert("Member not found!", "Find Member Error!", true, true);
					resetForm();
				}
			}
		});
	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetForm() {
		btnUpdate.setEnabled(false);
		fieldName.setEnabled(false);
		fieldName.setText("");
		fieldFamily.setEnabled(false);
		fieldFamily.setText("");
		spinnerYear.setEnabled(false);
		spinnerYear.setValue(0);
		spinnerMonth.setEnabled(false);
		spinnerMonth.setValue("");
		spinnerDay.setEnabled(false);
		spinnerDay.setValue(0);
		rdbtnMale.setEnabled(false);
		rdbtnMale.setSelected(false);
		rdbtnFemale.setEnabled(false);
		rdbtnFemale.setSelected(false);
	}
}
