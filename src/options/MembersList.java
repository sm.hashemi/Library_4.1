package options;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import java.awt.GridBagConstraints;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Member;

public class MembersList {

	private static JFrame frameMembersFounded;
	private static JTable table;
	@SuppressWarnings("serial")
	private static DefaultTableModel model = new DefaultTableModel(0, 7) {
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameFoundedMember();
		frameMembersFounded.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameFoundedMember() {
		frameMembersFounded = new JFrame();
		frameMembersFounded.setTitle("Members List");
		frameMembersFounded.setBounds(100, 100, 1100, 500);
		frameMembersFounded.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frameMembersFounded.getContentPane().setLayout(gridBagLayout);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		frameMembersFounded.getContentPane().add(scrollPane, gbc_scrollPane);

		table = new JTable();
		String[] cols = { "Member ID", "Name", "Family", "Gender", "Birthdate", "Is Borrower?", "Borrowed Book ID", "Is Delayer?", "Penalty" };
		model.setColumnIdentifiers(cols);
		table.setModel(model);
		scrollPane.setViewportView(table);
	}

	/**
	 * Add Row to table with object data
	 */
	public static void addTableRow(Member m) {
		String isBorrow = null;
		String isDelay = null;
		if (m.isBorrower()) {
			isBorrow = "✔";
			if (m.isDelayer())
				isDelay = "✔";
			else
				isDelay = "✖";
		} else
			isBorrow = "✖";
		// add to row
		if (m.isBorrower()) {
			if (m.isDelayer()) {
				model.addRow(new String[] { String.valueOf(m.getId()), m.getName(), m.getFamily(), m.getGenderString(), "13" + m.getBirthString(), isBorrow, String.valueOf(m.getBorrowedBookId()), isDelay, "$ " + String.valueOf(m.getPenalty()) });
			} else {
				model.addRow(new String[] { String.valueOf(m.getId()), m.getName(), m.getFamily(), m.getGenderString(), "13" + m.getBirthString(), isBorrow, String.valueOf(m.getBorrowedBookId()), isDelay });
			}
		} else {
			model.addRow(new String[] { String.valueOf(m.getId()), m.getName(), m.getFamily(), m.getGenderString(), "13" + m.getBirthString(), isBorrow });
		}

	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetTable() {
		model.setRowCount(0);
	}
}
