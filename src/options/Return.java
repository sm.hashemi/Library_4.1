package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Cursor;

import model.*;

import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.SystemColor;

public class Return {

	private static JFrame frameReturn;
	private static JTextField fieldMemberFamily;
	private static JTextField fieldMemberName;
	private static JButton btnReturn;
	private static JTextField fieldMemberId;
	private static JTextField fieldBookId;
	private static JTextField fieldBookName;
	private static JTextField fieldBookAuthor;
	private static JCheckBox checkboxMember;
	private static JCheckBox checkboxBook;
	private static JTextField fieldMemberBorrowedBookId;
	private static JTextField fieldBookBorrowerId;
	private static JTextField fieldBookBorrowerName;
	private static JTextField fieldMemberBorrowedBookName;
	private static Member foundM;
	private static Book foundB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		initializeFrameReturn();
		frameReturn.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameReturn() {
		frameReturn = new JFrame();
		frameReturn.setTitle("Return Book");
		frameReturn.setVisible(true);
		frameReturn.setBounds(100, 100, 472, 472);
		frameReturn.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 36, 80, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -16, 0, 0, 0,
				0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		frameReturn.getContentPane().setLayout(gridBagLayout);

		JCheckBox chboxMember = new JCheckBox("");
		chboxMember.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_chboxMember = new GridBagConstraints();
		gbc_chboxMember.insets = new Insets(0, 0, 5, 5);
		gbc_chboxMember.gridx = 0;
		gbc_chboxMember.gridy = 0;
		frameReturn.getContentPane().add(chboxMember, gbc_chboxMember);

		JLabel lblMemberId = new JLabel("Member ID :");
		lblMemberId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_lblMemberId = new GridBagConstraints();
		gbc_lblMemberId.anchor = GridBagConstraints.EAST;
		gbc_lblMemberId.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberId.gridx = 1;
		gbc_lblMemberId.gridy = 0;
		frameReturn.getContentPane().add(lblMemberId, gbc_lblMemberId);

		fieldMemberId = new JTextField();
		fieldMemberId.setEnabled(false);
		;
		GridBagConstraints gbc_fieldMemberId = new GridBagConstraints();
		gbc_fieldMemberId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberId.gridx = 2;
		gbc_fieldMemberId.gridy = 0;
		frameReturn.getContentPane().add(fieldMemberId, gbc_fieldMemberId);
		fieldMemberId.setColumns(10);

		JButton btnFindMember = new JButton("Find Member!");
		btnFindMember.setEnabled(false);
		btnFindMember.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_btnFindMember = new GridBagConstraints();
		gbc_btnFindMember.anchor = GridBagConstraints.WEST;
		gbc_btnFindMember.insets = new Insets(0, 0, 5, 0);
		gbc_btnFindMember.gridx = 3;
		gbc_btnFindMember.gridy = 0;
		frameReturn.getContentPane().add(btnFindMember, gbc_btnFindMember);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 1;
		gbc_horizontalStrut_11.gridy = 1;
		frameReturn.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 2;
		gbc_horizontalStrut_9.gridy = 2;
		frameReturn.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		JLabel lblMemberName = new JLabel("Name :");
		lblMemberName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		GridBagConstraints gbc_lblMemberName = new GridBagConstraints();
		gbc_lblMemberName.anchor = GridBagConstraints.EAST;
		gbc_lblMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberName.gridx = 1;
		gbc_lblMemberName.gridy = 3;
		frameReturn.getContentPane().add(lblMemberName, gbc_lblMemberName);

		fieldMemberName = new JTextField();
		fieldMemberName.setEditable(false);
		GridBagConstraints gbc_fieldMemberName = new GridBagConstraints();
		gbc_fieldMemberName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberName.gridx = 2;
		gbc_fieldMemberName.gridy = 3;
		frameReturn.getContentPane().add(fieldMemberName, gbc_fieldMemberName);
		fieldMemberName.setColumns(10);

		JPanel panelCheckMember = new JPanel();
		GridBagConstraints gbc_panelCheckMember = new GridBagConstraints();
		gbc_panelCheckMember.anchor = GridBagConstraints.WEST;
		gbc_panelCheckMember.gridheight = 2;
		gbc_panelCheckMember.insets = new Insets(0, 0, 5, 0);
		gbc_panelCheckMember.gridx = 3;
		gbc_panelCheckMember.gridy = 3;
		frameReturn.getContentPane().add(panelCheckMember, gbc_panelCheckMember);

		checkboxMember = new JCheckBox("");
		checkboxMember.setBackground(SystemColor.menu);
		checkboxMember.setEnabled(false);
		panelCheckMember.add(checkboxMember);

		JLabel lblIsMemberBorrower = new JLabel("Is Borrower?");
		panelCheckMember.add(lblIsMemberBorrower);

		JLabel lblMemberFamily = new JLabel("Family :");
		lblMemberFamily.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblMemberFamily = new GridBagConstraints();
		gbc_lblMemberFamily.anchor = GridBagConstraints.EAST;
		gbc_lblMemberFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberFamily.gridx = 1;
		gbc_lblMemberFamily.gridy = 4;
		frameReturn.getContentPane().add(lblMemberFamily, gbc_lblMemberFamily);

		fieldMemberFamily = new JTextField();
		fieldMemberFamily.setEditable(false);
		GridBagConstraints gbc_fieldMemberFamily = new GridBagConstraints();
		gbc_fieldMemberFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberFamily.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberFamily.gridx = 2;
		gbc_fieldMemberFamily.gridy = 4;
		frameReturn.getContentPane().add(fieldMemberFamily, gbc_fieldMemberFamily);
		fieldMemberFamily.setColumns(10);

		JLabel lblMemberBorrowedBookID = new JLabel("Bor Book ID :");
		GridBagConstraints gbc_lblMemberBorrowedBookID = new GridBagConstraints();
		gbc_lblMemberBorrowedBookID.anchor = GridBagConstraints.EAST;
		gbc_lblMemberBorrowedBookID.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberBorrowedBookID.gridx = 1;
		gbc_lblMemberBorrowedBookID.gridy = 5;
		frameReturn.getContentPane().add(lblMemberBorrowedBookID, gbc_lblMemberBorrowedBookID);

		fieldMemberBorrowedBookId = new JTextField();
		fieldMemberBorrowedBookId.setEditable(false);
		GridBagConstraints gbc_fieldMemberBorId = new GridBagConstraints();
		gbc_fieldMemberBorId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberBorId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberBorId.gridx = 2;
		gbc_fieldMemberBorId.gridy = 5;
		frameReturn.getContentPane().add(fieldMemberBorrowedBookId, gbc_fieldMemberBorId);
		fieldMemberBorrowedBookId.setColumns(10);

		JLabel lblMemberBorrowedBookName = new JLabel("Bor Book Name :");
		GridBagConstraints gbc_lblMemberBorrowedBookName = new GridBagConstraints();
		gbc_lblMemberBorrowedBookName.anchor = GridBagConstraints.EAST;
		gbc_lblMemberBorrowedBookName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberBorrowedBookName.gridx = 1;
		gbc_lblMemberBorrowedBookName.gridy = 6;
		frameReturn.getContentPane().add(lblMemberBorrowedBookName, gbc_lblMemberBorrowedBookName);

		fieldMemberBorrowedBookName = new JTextField();
		fieldMemberBorrowedBookName.setEditable(false);
		GridBagConstraints gbc_fieldMemberBorrowedBookName = new GridBagConstraints();
		gbc_fieldMemberBorrowedBookName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldMemberBorrowedBookName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldMemberBorrowedBookName.gridx = 2;
		gbc_fieldMemberBorrowedBookName.gridy = 6;
		frameReturn.getContentPane().add(fieldMemberBorrowedBookName, gbc_fieldMemberBorrowedBookName);
		fieldMemberBorrowedBookName.setColumns(10);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 7;
		frameReturn.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel label = new JLabel("__________________________________________");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridwidth = 3;
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 1;
		gbc_label.gridy = 8;
		frameReturn.getContentPane().add(label, gbc_label);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 9;
		frameReturn.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 10;
		frameReturn.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 11;
		frameReturn.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		JCheckBox chboxBook = new JCheckBox("");
		chboxBook.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_chboxBook = new GridBagConstraints();
		gbc_chboxBook.insets = new Insets(0, 0, 5, 5);
		gbc_chboxBook.gridx = 0;
		gbc_chboxBook.gridy = 12;
		frameReturn.getContentPane().add(chboxBook, gbc_chboxBook);

		JLabel lblBookId = new JLabel("Book ID :");
		lblBookId.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_lblBookId = new GridBagConstraints();
		gbc_lblBookId.anchor = GridBagConstraints.EAST;
		gbc_lblBookId.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookId.gridx = 1;
		gbc_lblBookId.gridy = 12;
		frameReturn.getContentPane().add(lblBookId, gbc_lblBookId);

		fieldBookId = new JTextField();
		fieldBookId.setEnabled(false);
		fieldBookId.setColumns(10);
		GridBagConstraints gbc_fieldBookId = new GridBagConstraints();
		gbc_fieldBookId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookId.gridx = 2;
		gbc_fieldBookId.gridy = 12;
		frameReturn.getContentPane().add(fieldBookId, gbc_fieldBookId);

		// __________________Update Button_________________

		btnReturn = new JButton("Return!");
		btnReturn.setEnabled(false);
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chboxMember.isSelected()) { // fix later! reset delay & penalty
					Book b=(Book)Library.book.searchById(foundM.getBorrowedBookId());
					b.setAsReturned();
					b.setBorrowerId(0);
					foundM.setAsReturner();
					foundM.setBorrowedBookId(0);
					Logger.logReturn(foundM.getId(), b.getId());
				} else if (chboxBook.isSelected()) {
					Member m=(Member)Library.member.searchById(foundB.getBorrowerId());
					m.setAsReturner();
					m.setBorrowedBookId(0);
					foundB.setAsReturned();
					foundB.setBorrowerId(0);
					Logger.logReturn(m.getId(), foundB.getId());
				}
				Library.alert("✔ Registered Successfully!", "Return Done!", false, true);
				resetMemberForm();
				resetBookForm();
				btnReturn.setEnabled(false);
			}
		});

		JButton btnFindBook = new JButton("Find Book!");
		btnFindBook.setEnabled(false);
		btnFindBook.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnFindBook.setMinimumSize(new Dimension(111, 25));
		btnFindBook.setPreferredSize(new Dimension(111, 25));
		GridBagConstraints gbc_btnFindBook = new GridBagConstraints();
		gbc_btnFindBook.anchor = GridBagConstraints.WEST;
		gbc_btnFindBook.insets = new Insets(0, 0, 5, 0);
		gbc_btnFindBook.gridx = 3;
		gbc_btnFindBook.gridy = 12;
		frameReturn.getContentPane().add(btnFindBook, gbc_btnFindBook);

		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_10 = new GridBagConstraints();
		gbc_horizontalStrut_10.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_10.gridx = 2;
		gbc_horizontalStrut_10.gridy = 13;
		frameReturn.getContentPane().add(horizontalStrut_10, gbc_horizontalStrut_10);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 2;
		gbc_horizontalStrut_5.gridy = 14;
		frameReturn.getContentPane().add(horizontalStrut_5, gbc_horizontalStrut_5);

		JLabel lblBookName = new JLabel("Name :");
		GridBagConstraints gbc_lblBookName = new GridBagConstraints();
		gbc_lblBookName.anchor = GridBagConstraints.EAST;
		gbc_lblBookName.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookName.gridx = 1;
		gbc_lblBookName.gridy = 15;
		frameReturn.getContentPane().add(lblBookName, gbc_lblBookName);

		fieldBookName = new JTextField();
		fieldBookName.setEditable(false);
		fieldBookName.setColumns(10);
		GridBagConstraints gbc_fieldBookName = new GridBagConstraints();
		gbc_fieldBookName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookName.gridx = 2;
		gbc_fieldBookName.gridy = 15;
		frameReturn.getContentPane().add(fieldBookName, gbc_fieldBookName);

		JLabel lblBookAuthor = new JLabel("Author :");
		GridBagConstraints gbc_lblBookAuthor = new GridBagConstraints();
		gbc_lblBookAuthor.anchor = GridBagConstraints.EAST;
		gbc_lblBookAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookAuthor.gridx = 1;
		gbc_lblBookAuthor.gridy = 16;
		frameReturn.getContentPane().add(lblBookAuthor, gbc_lblBookAuthor);

		fieldBookAuthor = new JTextField();
		fieldBookAuthor.setEditable(false);
		fieldBookAuthor.setColumns(10);
		GridBagConstraints gbc_fieldBookAuthor = new GridBagConstraints();
		gbc_fieldBookAuthor.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookAuthor.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookAuthor.gridx = 2;
		gbc_fieldBookAuthor.gridy = 16;
		frameReturn.getContentPane().add(fieldBookAuthor, gbc_fieldBookAuthor);

		JPanel panelCheckBook = new JPanel();
		GridBagConstraints gbc_panelCheckBook = new GridBagConstraints();
		gbc_panelCheckBook.anchor = GridBagConstraints.WEST;
		gbc_panelCheckBook.gridheight = 2;
		gbc_panelCheckBook.insets = new Insets(0, 0, 5, 0);
		gbc_panelCheckBook.gridx = 3;
		gbc_panelCheckBook.gridy = 15;
		frameReturn.getContentPane().add(panelCheckBook, gbc_panelCheckBook);

		checkboxBook = new JCheckBox("");
		checkboxBook.setEnabled(false);
		panelCheckBook.add(checkboxBook);

		JLabel lblisBookBorrowed = new JLabel("Is Borrowed?");
		panelCheckBook.add(lblisBookBorrowed);

		JLabel lblBookBorrowerId = new JLabel("Borrower ID :\r\n");
		GridBagConstraints gbc_lblBookBorrowerId = new GridBagConstraints();
		gbc_lblBookBorrowerId.anchor = GridBagConstraints.EAST;
		gbc_lblBookBorrowerId.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookBorrowerId.gridx = 1;
		gbc_lblBookBorrowerId.gridy = 17;
		frameReturn.getContentPane().add(lblBookBorrowerId, gbc_lblBookBorrowerId);

		fieldBookBorrowerId = new JTextField();
		fieldBookBorrowerId.setEditable(false);
		GridBagConstraints gbc_fieldBookBorrowerId = new GridBagConstraints();
		gbc_fieldBookBorrowerId.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookBorrowerId.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookBorrowerId.gridx = 2;
		gbc_fieldBookBorrowerId.gridy = 17;
		frameReturn.getContentPane().add(fieldBookBorrowerId, gbc_fieldBookBorrowerId);
		fieldBookBorrowerId.setColumns(10);

		JLabel lblBookBorrowerName = new JLabel("Borrower Name :");
		GridBagConstraints gbc_lblBookBorrowerName = new GridBagConstraints();
		gbc_lblBookBorrowerName.anchor = GridBagConstraints.EAST;
		gbc_lblBookBorrowerName.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookBorrowerName.gridx = 1;
		gbc_lblBookBorrowerName.gridy = 18;
		frameReturn.getContentPane().add(lblBookBorrowerName, gbc_lblBookBorrowerName);

		fieldBookBorrowerName = new JTextField();
		fieldBookBorrowerName.setEditable(false);
		GridBagConstraints gbc_fieldBookBorrowerName = new GridBagConstraints();
		gbc_fieldBookBorrowerName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldBookBorrowerName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldBookBorrowerName.gridx = 2;
		gbc_fieldBookBorrowerName.gridy = 18;
		frameReturn.getContentPane().add(fieldBookBorrowerName, gbc_fieldBookBorrowerName);
		fieldBookBorrowerName.setColumns(10);

		Component horizontalStrut_17 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_17 = new GridBagConstraints();
		gbc_horizontalStrut_17.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_17.gridx = 1;
		gbc_horizontalStrut_17.gridy = 19;
		frameReturn.getContentPane().add(horizontalStrut_17, gbc_horizontalStrut_17);

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 20;
		frameReturn.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_15 = new GridBagConstraints();
		gbc_horizontalStrut_15.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_15.gridx = 1;
		gbc_horizontalStrut_15.gridy = 21;
		frameReturn.getContentPane().add(horizontalStrut_15, gbc_horizontalStrut_15);

		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_14 = new GridBagConstraints();
		gbc_horizontalStrut_14.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_14.gridx = 1;
		gbc_horizontalStrut_14.gridy = 22;
		frameReturn.getContentPane().add(horizontalStrut_14, gbc_horizontalStrut_14);

		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_12 = new GridBagConstraints();
		gbc_horizontalStrut_12.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_12.gridx = 2;
		gbc_horizontalStrut_12.gridy = 23;
		frameReturn.getContentPane().add(horizontalStrut_12, gbc_horizontalStrut_12);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 24;
		frameReturn.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);
		btnReturn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnReturn = new GridBagConstraints();
		gbc_btnReturn.gridwidth = 4;
		gbc_btnReturn.gridx = 0;
		gbc_btnReturn.gridy = 25;
		frameReturn.getContentPane().add(btnReturn, gbc_btnReturn);

		btnFindMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldMemberId.getText();
				int idInt;
				// validate
				if (id.equals("")) {
					Library.alert("Please enter \"Member ID\"!", "Find Member Error!", true, true);
					resetMemberForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 1000 || idInt > Member.getIdIndex()) {
						Library.alert("Invalid \"ID range\"!", "Find Member Error!", true, true);
						resetMemberForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"ID number\"!", "Find Member Error!", true, true);
					resetMemberForm();
					return;
				}
				// search
				if (Library.member.searchById(idInt) != null) {
					foundM = (Member) Library.member.searchById(idInt);
					fieldMemberName.setText(foundM.getName());
					fieldMemberFamily.setText(foundM.getFamily());
					if (foundM.isBorrower()) {
						btnReturn.setEnabled(true);
						checkboxMember.setBackground(Color.GREEN);
						checkboxMember.setSelected(true);
						fieldMemberBorrowedBookId.setText(String.valueOf(foundM.getBorrowedBookId()));
						// find name of borrowed book // fix later! probably
						// book deleted
						Book borrowedBook = (Book) Library.book.searchById(foundM.getBorrowedBookId());
						fieldMemberBorrowedBookName.setText(borrowedBook.getName());
					} else {
						checkboxMember.setBackground(Color.RED);
					}
					return;
				} else {
					Library.alert("Member not found!", "Find Member Error!", true, true);
					resetMemberForm();
				}
			}
		});

		btnFindBook.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// get id from field
				String id = fieldBookId.getText();
				int idInt;
				// validate
				if (id.equals("")) {
					Library.alert("Please enter \"Book ID\"!", "Find Book Error!", true, true);
					resetBookForm();
					return;
				}
				try {
					idInt = Integer.parseInt(id);
					if (idInt < 100 || idInt > Book.getIdIndex()) {
						Library.alert("Invalid \"ID range\"!", "Find Book Error!", true, true);
						resetBookForm();
						return;
					}
				} catch (NumberFormatException e) {
					Library.alert("Invalid \"ID number\"!", "Find Book Error!", true, true);
					resetBookForm();
					return;
				}
				// search
				if (Library.book.searchById(idInt) != null) {
					foundB = (Book) Library.book.searchById(idInt);
					fieldBookName.setText(foundB.getName());
					fieldBookAuthor.setText(foundB.getAuthor());
					if (foundB.isBorrowed()) {
						btnReturn.setEnabled(true);
						checkboxBook.setBackground(Color.GREEN);
						checkboxBook.setSelected(true);
						fieldBookBorrowerId.setText(String.valueOf(foundB.getBorrowerId()));
						// find name of borrower // fix later! probably member
						// deleted
						Member borrower = (Member) Library.member.searchById(foundB.getBorrowerId());
						fieldBookBorrowerName.setText(borrower.getName());
					} else {
						checkboxBook.setBackground(Color.RED);
					}
					return;
				} else {
					Library.alert("Book not found!", "Find Book Error!", true, true);
					resetBookForm();
				}
			}

		});

		chboxMember.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					// enable member
					lblMemberId.setEnabled(true);
					fieldMemberId.setEnabled(true);
					btnFindMember.setEnabled(true);
					// disable book
					lblBookId.setEnabled(false);
					chboxBook.setSelected(false);
				}
				if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					btnReturn.setEnabled(false);
					// disable member
					btnFindMember.setEnabled(false);
					fieldMemberId.setEnabled(false);
					// enable book
					lblBookId.setEnabled(true);
				}
			}
		});

		chboxBook.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					// enable book
					btnFindBook.setEnabled(true);
					fieldBookId.setEnabled(true);
					lblBookId.setEnabled(true);
					// disable member
					lblMemberId.setEnabled(false);
					chboxMember.setSelected(false);

				}
				if (arg0.getStateChange() == ItemEvent.DESELECTED) {
					btnReturn.setEnabled(false);
					// disable book
					fieldBookId.setEnabled(false);
					btnFindBook.setEnabled(false);
					// enable member
					lblMemberId.setEnabled(true);
				}
			}
		});

		lblMemberId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!chboxMember.isSelected()) {
					chboxMember.setSelected(true);
				} else if (chboxMember.isSelected()) {
					chboxMember.setSelected(false);
				}
			}
		});

		lblBookId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!chboxBook.isSelected()) {
					chboxBook.setSelected(true);
				} else if (chboxBook.isSelected()) {
					chboxBook.setSelected(false);
				}
			}
		});
	}

	/**
	 * Reset the contents of the member frame.
	 */
	public static void resetMemberForm() {
		checkboxMember.setBackground(SystemColor.menu);
		checkboxMember.setSelected(false);
		fieldMemberName.setText("");
		fieldMemberFamily.setText("");
		fieldMemberBorrowedBookId.setText("");
		fieldMemberBorrowedBookName.setText("");
	}

	/**
	 * Reset the contents of the book frame.
	 */
	public static void resetBookForm() {
		checkboxBook.setBackground(SystemColor.menu);
		checkboxBook.setSelected(false);
		fieldBookName.setText("");
		fieldBookAuthor.setText("");
		fieldBookBorrowerId.setText("");
		fieldBookBorrowerName.setText("");
	}

}
