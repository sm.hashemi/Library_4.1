package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Cursor;

import model.Library;
import model.Logger;

import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class SelfAdminPass {

	private static JFrame frameUpdateAdmin;
	private static JButton btnChange;
	private static JPasswordField fieldOld;
	private static JPasswordField fieldNew;
	private static JPasswordField fieldReNew;

	/**
	 * Launch the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public static void main(String[] args) {
		initializeFrameUpdateAdmin();
		frameUpdateAdmin.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameUpdateAdmin() {
		frameUpdateAdmin = new JFrame();
		frameUpdateAdmin.setTitle("Change Admin Password");
		frameUpdateAdmin.setVisible(true);
		frameUpdateAdmin.setBounds(100, 100, 472, 313);
		frameUpdateAdmin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 36, 109, 59 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		frameUpdateAdmin.getContentPane().setLayout(gridBagLayout);

		JLabel lblOld = new JLabel("Old Password :");
		lblOld.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblOld = new GridBagConstraints();
		gbc_lblOld.anchor = GridBagConstraints.EAST;
		gbc_lblOld.insets = new Insets(0, 0, 5, 5);
		gbc_lblOld.gridx = 0;
		gbc_lblOld.gridy = 0;
		frameUpdateAdmin.getContentPane().add(lblOld, gbc_lblOld);
		;

		fieldOld = new JPasswordField();
		GridBagConstraints gbc_fieldOld = new GridBagConstraints();
		gbc_fieldOld.insets = new Insets(0, 0, 5, 5);
		gbc_fieldOld.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldOld.gridx = 1;
		gbc_fieldOld.gridy = 0;
		frameUpdateAdmin.getContentPane().add(fieldOld, gbc_fieldOld);

		JButton btnCheck = new JButton("Check");
		btnCheck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		GridBagConstraints gbc_btnCheck = new GridBagConstraints();
		gbc_btnCheck.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCheck.insets = new Insets(0, 0, 5, 0);
		gbc_btnCheck.gridx = 2;
		gbc_btnCheck.gridy = 0;
		frameUpdateAdmin.getContentPane().add(btnCheck, gbc_btnCheck);

		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_11 = new GridBagConstraints();
		gbc_horizontalStrut_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_11.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_11.gridx = 0;
		gbc_horizontalStrut_11.gridy = 1;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_11, gbc_horizontalStrut_11);

		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_9 = new GridBagConstraints();
		gbc_horizontalStrut_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_9.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_9.gridx = 1;
		gbc_horizontalStrut_9.gridy = 2;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_9, gbc_horizontalStrut_9);

		JLabel lblNew = new JLabel("New Password :");
		lblNew.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		GridBagConstraints gbc_lblNew = new GridBagConstraints();
		gbc_lblNew.anchor = GridBagConstraints.EAST;
		gbc_lblNew.insets = new Insets(0, 0, 5, 5);
		gbc_lblNew.gridx = 0;
		gbc_lblNew.gridy = 3;
		frameUpdateAdmin.getContentPane().add(lblNew, gbc_lblNew);

		fieldNew = new JPasswordField();
		fieldNew.setEnabled(false);
		GridBagConstraints gbc_fieldNew = new GridBagConstraints();
		gbc_fieldNew.insets = new Insets(0, 0, 5, 5);
		gbc_fieldNew.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldNew.gridx = 1;
		gbc_fieldNew.gridy = 3;
		frameUpdateAdmin.getContentPane().add(fieldNew, gbc_fieldNew);

		JLabel lblReNew = new JLabel("Re-New Password :");
		lblReNew.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblReNew = new GridBagConstraints();
		gbc_lblReNew.anchor = GridBagConstraints.EAST;
		gbc_lblReNew.insets = new Insets(0, 0, 5, 5);
		gbc_lblReNew.gridx = 0;
		gbc_lblReNew.gridy = 4;
		frameUpdateAdmin.getContentPane().add(lblReNew, gbc_lblReNew);

		fieldReNew = new JPasswordField();
		fieldReNew.setEnabled(false);
		GridBagConstraints gbc_fieldReNew = new GridBagConstraints();
		gbc_fieldReNew.insets = new Insets(0, 0, 5, 5);
		gbc_fieldReNew.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldReNew.gridx = 1;
		gbc_fieldReNew.gridy = 4;
		frameUpdateAdmin.getContentPane().add(fieldReNew, gbc_fieldReNew);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 5;
		frameUpdateAdmin.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 1;
		gbc_horizontalStrut_6.gridy = 6;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 1;
		gbc_horizontalStrut_4.gridy = 7;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		// __________________Update Button_________________

		btnChange = new JButton("Change Password!");
		btnChange.setEnabled(false);
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String password = String.valueOf(fieldNew.getPassword());
				String passwordRe = String.valueOf(fieldReNew.getPassword());
				if (password.equals("")) {
					Library.alert("✖ Plese enter New Password!", "Error!", true, true);
					return;
				}
				if (passwordRe.equals("")) {
					Library.alert("✖ Plese enter New Password again!", "Error!", true, true);
					return;
				}
				if (password.equals(passwordRe)) {
					Library.setAdminPassword(password);
					Logger.logAdminP(password);
					Library.alert("✔ Password changed successfully!", "Done!", false, true);
					resetForm();
				} else {
					Library.alert("✖ New Passwords don't match!", "Error!", true, true);
				}

			}
		});

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 1;
		gbc_horizontalStrut_8.gridy = 8;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_15 = new GridBagConstraints();
		gbc_horizontalStrut_15.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_15.gridx = 0;
		gbc_horizontalStrut_15.gridy = 9;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_15, gbc_horizontalStrut_15);

		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_14 = new GridBagConstraints();
		gbc_horizontalStrut_14.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_14.gridx = 0;
		gbc_horizontalStrut_14.gridy = 10;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_14, gbc_horizontalStrut_14);

		Component horizontalStrut_13 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_13 = new GridBagConstraints();
		gbc_horizontalStrut_13.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_13.gridx = 1;
		gbc_horizontalStrut_13.gridy = 11;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_13, gbc_horizontalStrut_13);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 0;
		gbc_horizontalStrut_3.gridy = 12;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 0;
		gbc_horizontalStrut_2.gridy = 13;
		frameUpdateAdmin.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);
		btnChange.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnChange = new GridBagConstraints();
		gbc_btnChange.gridwidth = 3;
		gbc_btnChange.gridx = 0;
		gbc_btnChange.gridy = 14;
		frameUpdateAdmin.getContentPane().add(btnChange, gbc_btnChange);

		btnCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String password = String.valueOf(fieldOld.getPassword());
				if (password.equals(Library.getAdminPassword())) {
					btnChange.setEnabled(true);
					fieldNew.setEnabled(true);
					fieldReNew.setEnabled(true);
				} else
					return;
			}

		});
	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetForm() {
		fieldNew.setText("");
		fieldReNew.setText("");
		btnChange.setEnabled(false);
	}
}
