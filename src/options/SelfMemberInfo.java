package options;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import java.awt.Cursor;
import javax.swing.SpinnerNumberModel;

import model.*;

import javax.swing.SpinnerListModel;
import javax.swing.ButtonGroup;
import java.awt.event.ActionEvent;

public class SelfMemberInfo {

	private static JFrame frameUpdateMember;
	private static JTextField fieldFamily;
	private static JTextField fieldName;
	private static JButton btnUpdate;
	private static JSpinner spinnerYear;
	private static JSpinner spinnerMonth;
	private static JSpinner spinnerDay;
	private static JRadioButton rdbtnMale;
	private static JRadioButton rdbtnFemale;
	private final static ButtonGroup buttonGroup = new ButtonGroup();
	private static Member loggedInMember;

	/**
	 * Launch the application.
	 * @wbp.parser.entryPoint
	 */
	public static void main(Member m) {
		loggedInMember=m;
		initializeFrameUpdateMember();
		frameUpdateMember.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initializeFrameUpdateMember() {
		frameUpdateMember = new JFrame();
		frameUpdateMember.setTitle("Update Member");
		frameUpdateMember.setVisible(true);
		frameUpdateMember.setBounds(100, 100, 380, 400);
		frameUpdateMember.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 16, 36, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 25, 0, 0, 20, -8, 25, 0, 0, 0, 0,
				0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		frameUpdateMember.getContentPane().setLayout(gridBagLayout);
		;

		JLabel lblName = new JLabel("Name :");
		lblName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		frameUpdateMember.getContentPane().add(lblName, gbc_lblName);

		fieldName = new JTextField();
		GridBagConstraints gbc_fieldName = new GridBagConstraints();
		gbc_fieldName.insets = new Insets(0, 0, 5, 5);
		gbc_fieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldName.gridx = 2;
		gbc_fieldName.gridy = 1;
		frameUpdateMember.getContentPane().add(fieldName, gbc_fieldName);
		fieldName.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 1;
		gbc_horizontalStrut_2.gridy = 2;
		frameUpdateMember.getContentPane().add(horizontalStrut_2, gbc_horizontalStrut_2);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 3;
		frameUpdateMember.getContentPane().add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblFamily = new JLabel("Family :");
		lblFamily.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblFamily = new GridBagConstraints();
		gbc_lblFamily.anchor = GridBagConstraints.EAST;
		gbc_lblFamily.insets = new Insets(0, 0, 5, 5);
		gbc_lblFamily.gridx = 1;
		gbc_lblFamily.gridy = 4;
		frameUpdateMember.getContentPane().add(lblFamily, gbc_lblFamily);

		fieldFamily = new JTextField();
		GridBagConstraints gbc_fieldFamily = new GridBagConstraints();
		gbc_fieldFamily.insets = new Insets(0, 0, 5, 5);
		gbc_fieldFamily.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldFamily.gridx = 2;
		gbc_fieldFamily.gridy = 4;
		frameUpdateMember.getContentPane().add(fieldFamily, gbc_fieldFamily);
		fieldFamily.setColumns(10);

		// ________________________________________________

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 2;
		gbc_horizontalStrut_6.gridy = 5;
		frameUpdateMember.getContentPane().add(horizontalStrut_6, gbc_horizontalStrut_6);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 6;
		frameUpdateMember.getContentPane().add(horizontalStrut_4, gbc_horizontalStrut_4);

		JLabel lblBirth = new JLabel("Birth :");
		lblBirth.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblBirth = new GridBagConstraints();
		gbc_lblBirth.anchor = GridBagConstraints.EAST;
		gbc_lblBirth.insets = new Insets(0, 0, 5, 5);
		gbc_lblBirth.gridx = 1;
		gbc_lblBirth.gridy = 7;
		frameUpdateMember.getContentPane().add(lblBirth, gbc_lblBirth);

		// -----------Panel for 3 Birth spinner------------

		JPanel panelBirth = new JPanel();
		GridBagConstraints gbc_panelBirth = new GridBagConstraints();
		gbc_panelBirth.insets = new Insets(0, 0, 5, 5);
		gbc_panelBirth.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelBirth.gridx = 2;
		gbc_panelBirth.gridy = 7;
		frameUpdateMember.getContentPane().add(panelBirth, gbc_panelBirth);

		JLabel label = new JLabel("13");
		panelBirth.add(label);

		spinnerYear = new JSpinner();
		spinnerYear.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 90),
				new Byte((byte) 1)));
		spinnerYear.setPreferredSize(new Dimension(35, 20));
		spinnerYear.setMinimumSize(new Dimension(50, 20));
		panelBirth.add(spinnerYear);

		JLabel label_1 = new JLabel("/");
		panelBirth.add(label_1);

		spinnerMonth = new JSpinner();
		spinnerMonth.setModel(new SpinnerListModel(new String[] { "", "Farvardin", "Ordibehesht", "Khordad", "Tir",
				"Mordad", "Shahrivar", "Mehr", "Aban", "Azar", "Dey", "Bahman", "Esfand" }));
		spinnerMonth.setPreferredSize(new Dimension(80, 20));
		panelBirth.add(spinnerMonth);

		JLabel label_2 = new JLabel("/");
		panelBirth.add(label_2);

		spinnerDay = new JSpinner();
		spinnerDay.setModel(new SpinnerNumberModel(new Byte((byte) 0), new Byte((byte) 0), new Byte((byte) 30),
				new Byte((byte) 1)));
		spinnerDay.setPreferredSize(new Dimension(35, 20));
		panelBirth.add(spinnerDay);

		// ________________________________________________

		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_7.gridx = 2;
		gbc_horizontalStrut_7.gridy = 8;
		frameUpdateMember.getContentPane().add(horizontalStrut_7, gbc_horizontalStrut_7);

		JLabel lblGender = new JLabel("Gender :");
		lblGender.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		GridBagConstraints gbc_lblGender = new GridBagConstraints();
		gbc_lblGender.anchor = GridBagConstraints.EAST;
		gbc_lblGender.insets = new Insets(0, 0, 5, 5);
		gbc_lblGender.gridx = 1;
		gbc_lblGender.gridy = 9;
		frameUpdateMember.getContentPane().add(lblGender, gbc_lblGender);

		// ----------Panel for 2 Gen Radio Button----------

		JPanel panelGender = new JPanel();
		GridBagConstraints gbc_panelGender = new GridBagConstraints();
		gbc_panelGender.anchor = GridBagConstraints.WEST;
		gbc_panelGender.insets = new Insets(0, 0, 5, 5);
		gbc_panelGender.gridx = 2;
		gbc_panelGender.gridy = 9;
		frameUpdateMember.getContentPane().add(panelGender, gbc_panelGender);

		rdbtnMale = new JRadioButton("Male");
		buttonGroup.add(rdbtnMale);
		rdbtnMale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelGender.add(rdbtnMale);

		rdbtnFemale = new JRadioButton("Female\r\n");
		buttonGroup.add(rdbtnFemale);
		rdbtnFemale.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelGender.add(rdbtnFemale);

		// ________________________________________________

		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_8 = new GridBagConstraints();
		gbc_horizontalStrut_8.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_8.gridx = 2;
		gbc_horizontalStrut_8.gridy = 10;
		frameUpdateMember.getContentPane().add(horizontalStrut_8, gbc_horizontalStrut_8);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 11;
		frameUpdateMember.getContentPane().add(horizontalStrut_3, gbc_horizontalStrut_3);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 12;
		frameUpdateMember.getContentPane().add(horizontalStrut_1, gbc_horizontalStrut_1);

		// __________________Update Button_________________

		btnUpdate = new JButton("Update!");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get data
				String name = fieldName.getText();
				String family = fieldFamily.getText();
				byte year = (byte) spinnerYear.getValue();
				byte month = convertMonthToNum((String) spinnerMonth.getValue());
				byte day = (byte) spinnerDay.getValue();
				boolean gender;
				// Validate data
				if (name.equals("")) {
					Library.alert("Please Enter Name!", "Update Member Error!", true, true);
					return;
				}
				if (family.equals("")) {
					Library.alert("Please Enter Family!", "Update Member Error!", true, true);
					return;
				}
				if (year == 0 && month == 0 && day == 0) {
					Library.alert("Please Select Birth Date!", "Update Member Error!", true, true);
					return;
				}
				if (!rdbtnMale.isSelected() && !rdbtnFemale.isSelected()) {
					Library.alert("Please Select Gender!", "Update Member Error!", true, true);
					return;
				}
				if (rdbtnMale.isSelected())
					gender = true;
				else
					gender = false;
				// Update data
				loggedInMember.setName(name);
				loggedInMember.setFamily(family);
				loggedInMember.setBirth(year, month, day);
				loggedInMember.setGender(gender);
				Library.alert("✔ Data Updated Successfully!", "Done!", false, true);
				resetForm();
			}
		});
		btnUpdate.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 5);
		gbc_btnUpdate.gridwidth = 2;
		gbc_btnUpdate.gridx = 1;
		gbc_btnUpdate.gridy = 13;
		frameUpdateMember.getContentPane().add(btnUpdate, gbc_btnUpdate);
		
		// fill field with data
		fieldName.setText(loggedInMember.getName());
		fieldFamily.setText(loggedInMember.getFamily());
		spinnerYear.setValue(loggedInMember.getBirthYear());
		spinnerMonth.setValue(convertNumToMonth(loggedInMember.getBirthMonth()));
		spinnerDay.setValue(loggedInMember.getBirthDay());
		rdbtnMale.setEnabled(true);
		if (loggedInMember.getGender())
			rdbtnMale.setSelected(true);
		else
			rdbtnFemale.setSelected(true);
	}

	/**
	 * Convert month string to number
	 */
	public static byte convertMonthToNum(String month) {
		if (month.equals(""))
			return 0;
		else if (month.equals("Farvardin"))
			return 1;
		else if (month.equals("Ordibehesht"))
			return 2;
		else if (month.equals("Khordad"))
			return 3;
		else if (month.equals("Tir"))
			return 4;
		else if (month.equals("Mordad"))
			return 5;
		else if (month.equals("Shahrivar"))
			return 6;
		else if (month.equals("Mehr"))
			return 7;
		else if (month.equals("Aban"))
			return 8;
		else if (month.equals("Azar"))
			return 9;
		else if (month.equals("Dey"))
			return 10;
		else if (month.equals("Bahman"))
			return 11;
		else if (month.equals("Esfand"))
			return 12;
		return 0;
	}

	/**
	 * Convert month number to string
	 */
	public static String convertNumToMonth(int num) {
		if (num == 0)
			return "";
		else if (num == 1)
			return "Farvardin";
		else if (num == 2)
			return "Ordibehesht";
		else if (num == 3)
			return "Khordad";
		else if (num == 4)
			return "Tir";
		else if (num == 5)
			return "Mordad";
		else if (num == 6)
			return "Shahrivar";
		else if (num == 7)
			return "Mehr";
		else if (num == 8)
			return "Aban";
		else if (num == 9)
			return "Azar";
		else if (num == 10)
			return "Dey";
		else if (num == 11)
			return "Bahman";
		else if (num == 12)
			return "Esfand";
		return "";
	}

	/**
	 * Reset the contents of the frame.
	 */
	public static void resetForm() {
		btnUpdate.setEnabled(false);
		fieldName.setEnabled(false);
		fieldName.setText("");
		fieldFamily.setEnabled(false);
		fieldFamily.setText("");
		spinnerYear.setEnabled(false);
		spinnerYear.setValue(0);
		spinnerMonth.setEnabled(false);
		spinnerMonth.setValue("");
		spinnerDay.setEnabled(false);
		spinnerDay.setValue(0);
		rdbtnMale.setEnabled(false);
		rdbtnMale.setSelected(false);
		rdbtnFemale.setEnabled(false);
		rdbtnFemale.setSelected(false);
	}
}
